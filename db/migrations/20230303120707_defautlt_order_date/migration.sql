-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Order" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "date" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "status" TEXT NOT NULL DEFAULT 'created',
    "price" INTEGER NOT NULL,
    "quantity" INTEGER NOT NULL,
    "is_customer" BOOLEAN NOT NULL DEFAULT true,
    "woodId" INTEGER NOT NULL,
    "clientId" INTEGER,
    CONSTRAINT "Order_woodId_fkey" FOREIGN KEY ("woodId") REFERENCES "Wood" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Order_clientId_fkey" FOREIGN KEY ("clientId") REFERENCES "Client" ("id") ON DELETE SET NULL ON UPDATE CASCADE
);
INSERT INTO "new_Order" ("clientId", "createdAt", "date", "id", "is_customer", "price", "quantity", "status", "updatedAt", "woodId") SELECT "clientId", "createdAt", "date", "id", "is_customer", "price", "quantity", "status", "updatedAt", "woodId" FROM "Order";
DROP TABLE "Order";
ALTER TABLE "new_Order" RENAME TO "Order";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
