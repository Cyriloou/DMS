-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Client" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" DATETIME NOT NULL,
    "name" TEXT NOT NULL,
    "address" TEXT,
    "tel" TEXT,
    "email" TEXT,
    "role" TEXT NOT NULL
);
INSERT INTO "new_Client" ("address", "createdAt", "email", "id", "name", "role", "tel", "updatedAt") SELECT "address", "createdAt", "email", "id", "name", "role", "tel", "updatedAt" FROM "Client";
DROP TABLE "Client";
ALTER TABLE "new_Client" RENAME TO "Client";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
