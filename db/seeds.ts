import { SecurePassword } from "@blitzjs/auth"
import db from "./index"
import _ from "lodash"

const CLIENT_ROLES = {
  CLIENT: "CLIENT",
  SUPPLIER: "SUPPLIER",
}
const ORDER_STATUS = {
  CREATED: "created",
  REFUSED: "refused",
  ACCEPTED: "accepted",
  CANCELED: "canceled",
  DELIVERED: "delivered",
  PAID: "paid",
}
const statusList = Object.values(ORDER_STATUS)

// import db from "./index"
const USER_ROLES = {
  USER: "USER",
  ADMIN: "ADMIN",
  TRADER: "TRADER",
  COMMERCIAL: "COMMERCIAL",
}

/*
 * This seed function is executed when you run `blitz db seed`.
 *
 * Probably you want to use a library like https://chancejs.com
 * to easily generate realistic data.
 */

export default async () => {
  const users = await db.user.findMany({})
  if (!users.some((el) => el.email === "admin@free.fr")) {
    await db.user.create({
      data: {
        email: "admin@free.fr",
        hashedPassword: await SecurePassword.hash("123456".trim()),
        role: USER_ROLES.ADMIN,
      },
    })
  }

  await Promise.all(
    [
      "chestnut",
      "oak",
      "larch",
      "olive wood",
      "rosewood",
      "sycamore",
      "limewood",
      "wenge",
      "teak",
    ].map((name) =>
      db.wood.create({
        data: {
          name,
        },
      })
    )
  )

  // seed Supplier
  await Promise.all(
    _.times(50, _.constant(0)).map((el: number, index: number) => {
      const isClient = isOdd(_.random(0, 100))
      const name = isClient ? `Client ${index + 1}` : `Supplier ${index + 1}`
      return db.client.create({
        data: {
          name,
          role: isClient ? CLIENT_ROLES.CLIENT : CLIENT_ROLES.SUPPLIER,
          address: null,
          tel: null,
          email: null,
        },
      })
    })
  )

  const clients = await db.client.findMany({
    where: { role: CLIENT_ROLES.CLIENT },
  })
  const suppliers = await db.client.findMany({
    where: { role: CLIENT_ROLES.SUPPLIER },
  })
  const woods = await db.wood.findMany({})

  await Promise.all(
    _.times(4000, _.constant(0)).map(() => {
      const isClient = isOdd(_.random(0, 100))

      return db.order.create({
        data: {
          date: new Date(_.random(2022, 2023), _.random(1, 12), _.random(0, 28)),
          status: getRandomItem(statusList),
          price: _.random(1, 99) * 10,
          quantity: _.random(1, 9) * 100,
          is_customer: isClient,
          woodId: getRandomItem(woods)["id"],
          clientId: getRandomItem(isClient ? clients : suppliers)["id"],
        },
      })
    })
  )
}

function isOdd(n: number): boolean {
  return Math.abs(n % 2) == 1
}

function getRandomItem(array: Array<any>): any {
  return array.length ? array[Math.floor(Math.random() * array.length)] : null
}
