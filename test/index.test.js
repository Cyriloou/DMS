/**
 * @vitest-environment jsdom
 */

import { expect, vi, test } from "vitest"
import Home from "../src/pages/index"
import { render, screen } from "./utils"

vi.mock("public/logo.png", () => ({
  default: {
    src: "/logo.png",
  },
}))

test.skip("renders blitz documentation link", () => {
  // This is an example of how to ensure a specific item is in the document
  // But it's disabled by default (by test.skip) so the test doesn't fail
  // when you remove the default content from the page

  // This is an example on how to mock api hooks when testing
  vi.mock("src/users/hooks/useCurrentUser", () => ({
    useCurrentUser: () => ({
      id: 1,
      name: "User",
      email: "user@email.com",
      role: "user",
    }),
  }))
  render(<Home />)

  console.log(screen.debug())
  const linkElement = screen.getByText(/User id: 1/i)
  console.log(screen.debug())
  console.log("linkElement", linkElement)
  expect(linkElement).toBeInTheDocument()
})
