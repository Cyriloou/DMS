


## Environment Variables

Ensure the `.env` file has required environment variables:

```
DATABASE_URL="file:./db.sqlite"
LOCAL_STORAGE_NAME="customization"
NODE_ENV=development
```

## Seed database
yarn db:migrate
yarn db:seed

## First start up
```
yarn dev
```
http://localhost:3001/
go to home page and login admin@free.fr / 123456

## Tests

Runs your tests using Jest.

```
yarn test
```

Blitz comes with a test setup using [Vitest](https://vitest.dev/) and [react-testing-library](https://testing-library.com/).
