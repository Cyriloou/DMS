import { resolver } from "@blitzjs/rpc"
import db from "db"
import { CLIENT_ROLES } from "src/utils/enums"

export default resolver.pipe(resolver.authorize(), async () => {
  // TODO: in multi-tenant app, you must add validation to ensure correct tenant
  const clients = await db.client.findMany({
    where: { role: CLIENT_ROLES.SUPPLIER },
  })
  if (!clients) throw new NotFoundError()
  return clients.map((w) => ({
    value: w.id,
    label: w.name,
  }))
})
