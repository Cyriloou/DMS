import { NotFoundError } from "blitz"
import { resolver } from "@blitzjs/rpc"
import db from "db"
import { z } from "zod"
import { CLIENT_ROLES } from "src/utils/enums"
const GetClient = z.object({
  // This accepts type of undefined, but is required at runtime
  id: z.number().optional().refine(Boolean, "Required"),
})
export default resolver.pipe(resolver.zod(GetClient), resolver.authorize(), async ({ id }) => {
  // TODO: in multi-tenant app, you must add validation to ensure correct tenant
  const supplier = await db.client.findFirst({
    where: {
      id,
      role: CLIENT_ROLES.SUPPLIER,
    },
  })
  if (!supplier) throw new NotFoundError()
  return supplier
})
