import { Grid } from "@mui/material"
import { Form } from "src/core/components/Form"
import LabeledSelectField from "src/core/components/LabeledSelectField"
import { LabeledTextField } from "src/core/components/LabeledTextField"
import AuthCardWrapper from "src/pages/auth/AuthCardWrapper"
import { clientRoleOptions } from "src/utils/enums"
export { FORM_ERROR } from "src/core/components/Form"

export function ClientForm(props) {
  return (
    <AuthCardWrapper>
      <Form {...props}>
        <Grid item xs={12}>
          <LabeledTextField name="name" label="Name" placeholder="Name" />
        </Grid>
        <Grid item xs={12}>
          <LabeledTextField name="address" label="Address" placeholder="Address" />
        </Grid>
        <Grid item xs={12}>
          <LabeledTextField name="tel" label="Tel" placeholder="Tel" />
        </Grid>
        <Grid item xs={12}>
          <LabeledTextField name="email" label="Email" placeholder="Email" />
        </Grid>
        <Grid item xs={12}>
          <LabeledTextField name="role" label="Role" placeholder="Role" readOnly />
        </Grid>
        {/* <Grid item xs={12}>
          <LabeledSelectField
            name="role"
            label="Role"
            placeholder="Role"
            options={clientRoleOptions}
            // readOnly
          />
        </Grid> */}
      </Form>
    </AuthCardWrapper>
  )
}
