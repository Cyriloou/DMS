import { resolver } from "@blitzjs/rpc"
import db from "db"
import { CreateClient } from "src/auth/validations"

export default resolver.pipe(resolver.zod(CreateClient), resolver.authorize(), async (input) => {
  // TODO: in multi-tenant app, you must add validation to ensure correct tenant
  const client = await db.client.create({
    data: input,
  })
  return client
})
