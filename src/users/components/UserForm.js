import { Grid, Typography } from "@mui/material"
import { Box } from "@mui/system"
import { useState } from "react"
import { Form } from "src/core/components/Form"
import LabeledSelectField from "src/core/components/LabeledSelectField"
import { LabeledTextField } from "src/core/components/LabeledTextField"
import AuthCardWrapper from "src/pages/auth/AuthCardWrapper"
import { strengthColor, strengthIndicator } from "src/ui-component/password-strength"
import { userRoleOptions } from "src/utils/enums"
export { FORM_ERROR } from "src/core/components/Form"

export function UserForm(props) {
  const [strength, setStrength] = useState(0)
  const [level, setLevel] = useState()

  const changePassword = (value) => {
    const temp = strengthIndicator(value)
    setStrength(temp)
    setLevel(strengthColor(temp))
  }

  return (
    <AuthCardWrapper>
      <Form
        {...props}
        onChange={(event) => {
          const {
            target: { value, name },
          } = event
          if (name === "password") {
            changePassword(value)
          }
        }}
      >
        <Grid item xs={12}>
          <LabeledTextField name="email" label="Email" placeholder="Email" />
        </Grid>
        <Grid item xs={12}>
          <LabeledTextField
            name="password"
            label="Password"
            placeholder="Password"
            type="password"
          />
        </Grid>
        <Grid item xs={12}>
          <LabeledSelectField
            name="role"
            label="Role"
            placeholder="Role"
            options={userRoleOptions}
          />
        </Grid>
      </Form>
      {strength !== 0 && (
        <Box sx={{ mb: 2 }}>
          <Grid container spacing={2} alignItems="center">
            <Grid item>
              <Box
                style={{ backgroundColor: level?.color }}
                sx={{ width: 85, height: 8, borderRadius: "7px" }}
              />
            </Grid>
            <Grid item>
              <Typography variant="subtitle1" fontSize="0.75rem">
                {level?.label}
              </Typography>
            </Grid>
          </Grid>
        </Box>
      )}
    </AuthCardWrapper>
  )
}
