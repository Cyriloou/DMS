import { ORDER_STATUS, regexPhoneNumber } from "src/utils/constant"
import { CLIENT_ROLES, USER_ROLES } from "src/utils/enums"
import { z } from "zod"
export const id = z.number()
export const name = z.string().transform((str) => str.toLowerCase().trim())
export const address = z.string().transform((str) => str.toLowerCase().trim())
export const email = z
  .string()
  .email()
  .transform((str) => str.toLowerCase().trim())
export const password = z
  .string()
  .min(4)
  .max(50)
  .transform((str) => str.trim())

export const tel = z.string().regex(regexPhoneNumber).optional().or(z.literal(""))
export const price = z.number().positive()
export const quantity = z.number().positive()

export const userRole = z.enum(Object.values(USER_ROLES))
export const clientRole = z.enum(Object.values(CLIENT_ROLES))
export const orderStatus = z.enum(Object.values(ORDER_STATUS))
export const is_customer = z.boolean()
export const Signup = z.object({
  email,
  password,
})

export const Login = z.object({
  email,
  password: z.string(),
})

export const CreateWood = z.object({
  name,
})
export const UpdateWood = z.object({
  id,
  name,
})

export const CreateOrder = z.object({
  status: orderStatus,
  price,
  quantity,
  is_customer,
  woodId: id,
  clientId: id,
})
export const UpdateOrder = z.object({
  id,
  status: orderStatus,
  price,
  quantity,
  is_customer,
  woodId: id,
  clientId: id,
})

export const CreatePurchase = z.object({
  status: orderStatus,
  price,
  quantity,
  is_customer,
  woodId: id,
  clientId: id,
})
export const UpdatePurchase = z.object({
  id,
  status: orderStatus,
  price,
  quantity,
  is_customer,
  woodId: id,
  clientId: id,
})

export const CreateUser = z.object({
  email,
  password,
  role: userRole,
})
export const UpdateUser = z.object({
  id,
  email,
  password,
  role: userRole,
})

export const CreateClient = z.object({
  name,
  address,
  tel,
  email: email.or(z.literal("")),
  role: clientRole,
})
export const UpdateClient = z.object({
  id,
  name,
  address,
  tel,
  email: email.or(z.literal("")),
  role: clientRole,
})

export const CreateSupplier = z.object({
  name,
  address,
  tel,
  email: email.or(z.literal("")),
  role: clientRole,
})
export const UpdateSupplier = z.object({
  id,
  name,
  address,
  tel,
  email: email.or(z.literal("")),
  role: clientRole,
})

export const ForgotPassword = z.object({
  email,
})
export const ResetPassword = z
  .object({
    password: password,
    passwordConfirmation: password,
    token: z.string(),
  })
  .refine((data) => data.password === data.passwordConfirmation, {
    message: "Passwords don't match",
    path: ["passwordConfirmation"], // set the path of the error
  })

export const ChangePassword = z.object({
  currentPassword: z.string(),
  newPassword: password,
})
