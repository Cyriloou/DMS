import { LabeledTextField } from "src/core/components/LabeledTextField"
import { Form, FORM_ERROR } from "src/core/components/Form"
import signup from "src/auth/mutations/signup"
import { Signup } from "src/auth/validations"
import { useMutation } from "@blitzjs/rpc"
import AuthCardWrapper from "src/pages/auth/AuthCardWrapper"
import { Box, Grid, Stack, Typography, useMediaQuery, useTheme } from "@mui/material"
import { strengthColor, strengthIndicator } from "src/ui-component/password-strength"
import { useState } from "react"
import Link from "next/link"
import { Routes } from "@blitzjs/next"
import Logo from "src/ui-component/Logo"

export const SignupForm = (props) => {
  const theme = useTheme()
  const matchDownSM = useMediaQuery(theme.breakpoints.down("md"))
  const [signupMutation] = useMutation(signup)
  const [strength, setStrength] = useState(0)
  const [level, setLevel] = useState()

  const changePassword = (value) => {
    const temp = strengthIndicator(value)
    setStrength(temp)
    setLevel(strengthColor(temp))
  }

  return (
    <AuthCardWrapper>
      <Grid item sx={{ mb: 3 }}>
        <Link href={Routes.Home()}>
          <Logo />
        </Link>
      </Grid>
      <Grid item xs={12}>
        <Grid
          container
          direction={matchDownSM ? "column-reverse" : "row"}
          alignItems="center"
          justifyContent="center"
        >
          <Grid item>
            <Stack alignItems="center" justifyContent="center" spacing={1}>
              <Typography
                color={theme.palette.secondary.main}
                gutterBottom
                variant={matchDownSM ? "h3" : "h2"}
              >
                Sign up
              </Typography>
              <Typography
                variant="caption"
                fontSize="16px"
                textAlign={matchDownSM ? "center" : "inherit"}
              >
                Enter your credentials to continue
              </Typography>
            </Stack>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        {/* <AuthRegister /> */}
      </Grid>

      <Form
        submitText="Create Account"
        schema={Signup}
        initialValues={{
          email: "",
          password: "",
        }}
        onChange={(event) => {
          const {
            target: { value, name },
          } = event
          if (name === "password") {
            changePassword(value)
          }
        }}
        onSubmit={async (values) => {
          try {
            await signupMutation(values)
            props.onSuccess?.()
          } catch (error) {
            if (error.code === "P2002" && error.meta?.target?.includes("email")) {
              // This error comes from Prisma
              return {
                email: "This email is already being used",
              }
            } else {
              return {
                [FORM_ERROR]: error.toString(),
              }
            }
          }
        }}
      >
        <Grid item xs={12}>
          <LabeledTextField name="email" label="Email" placeholder="Email" />
        </Grid>
        <Grid item xs={12}>
          <LabeledTextField
            name="password"
            label="Password"
            placeholder="Password"
            type="password"
          />
        </Grid>
      </Form>
      {strength !== 0 && (
        <Box sx={{ mb: 2 }}>
          <Grid container spacing={2} alignItems="center">
            <Grid item>
              <Box
                style={{ backgroundColor: level?.color }}
                sx={{ width: 85, height: 8, borderRadius: "7px" }}
              />
            </Grid>
            <Grid item>
              <Typography variant="subtitle1" fontSize="0.75rem">
                {level?.label}
              </Typography>
            </Grid>
          </Grid>
        </Box>
      )}
    </AuthCardWrapper>
  )
}
export default SignupForm
