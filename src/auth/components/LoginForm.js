import { AuthenticationError } from "blitz"
import Link from "next/link"
import { LabeledTextField } from "src/core/components/LabeledTextField"
import { Form, FORM_ERROR } from "src/core/components/Form"
import login from "src/auth/mutations/login"
import { Login } from "src/auth/validations"
import { useMutation } from "@blitzjs/rpc"
import { Routes } from "@blitzjs/next"
import { Grid, Stack, Typography, useMediaQuery, useTheme } from "@mui/material"
import AuthCardWrapper from "src/pages/auth/AuthCardWrapper"
import Logo from "src/ui-component/Logo"

export const LoginForm = (props) => {
  const [loginMutation] = useMutation(login)
  const theme = useTheme()
  const matchDownSM = useMediaQuery(theme.breakpoints.down("md"))
  return (
    <AuthCardWrapper>
      <Grid item sx={{ mb: 3 }}>
        <Link href={Routes.Home()}>
          <Logo />
        </Link>
      </Grid>
      <Grid item xs={12}>
        <Grid
          container
          direction={matchDownSM ? "column-reverse" : "row"}
          alignItems="center"
          justifyContent="center"
        >
          <Grid item>
            <Stack alignItems="center" justifyContent="center" spacing={1}>
              <Typography
                color={theme.palette.secondary.main}
                gutterBottom
                variant={matchDownSM ? "h3" : "h2"}
              >
                Login
              </Typography>
              <Typography
                variant="caption"
                fontSize="16px"
                textAlign={matchDownSM ? "center" : "inherit"}
              >
                Enter your credentials to continue
              </Typography>
            </Stack>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12}>
        {/* <AuthRegister /> */}
      </Grid>

      <Form
        submitText="Login"
        schema={Login}
        initialValues={{
          email: "",
          password: "",
        }}
        onSubmit={async (values) => {
          try {
            const user = await loginMutation(values)
            props.onSuccess?.(user)
          } catch (error) {
            if (error instanceof AuthenticationError) {
              return {
                [FORM_ERROR]: "Sorry, those credentials are invalid",
              }
            } else {
              return {
                [FORM_ERROR]:
                  "Sorry, we had an unexpected error. Please try again. - " + error.toString(),
              }
            }
          }
        }}
      >
        <Grid item xs={12}>
          <LabeledTextField name="email" label="Email" placeholder="Email" />
        </Grid>
        <Grid item xs={12}>
          <LabeledTextField
            name="password"
            label="Password"
            placeholder="Password"
            type="password"
          />
        </Grid>
      </Form>

      <div>
        <Link href={Routes.ForgotPasswordPage()}>Forgot your password?</Link>
      </div>
      <div
        style={{
          marginTop: "1rem",
        }}
      >
        Or <Link href={Routes.SignupPage()}>Sign Up</Link>
      </div>
    </AuthCardWrapper>
  )
}
export default LoginForm
