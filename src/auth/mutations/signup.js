import { SecurePassword } from "@blitzjs/auth"
import { resolver } from "@blitzjs/rpc"
import db from "db"
import { USER_ROLES } from "src/utils/enums"
import { Signup } from "../validations"

export default resolver.pipe(
  resolver.zod(Signup),
  async ({ email, password, role = USER_ROLES.USER }, ctx) => {
    const hashedPassword = await SecurePassword.hash(password.trim())
    const user = await db.user.create({
      data: {
        email: email.toLowerCase().trim(),
        hashedPassword,
        role: role,
      },
      select: {
        id: true,
        name: true,
        email: true,
        role: true,
      },
    })
    await ctx.session.$create({
      userId: user.id,
      role: user.role,
    })
    return user
  }
)
