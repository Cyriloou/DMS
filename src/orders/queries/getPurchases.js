import { paginate } from "blitz"
import { resolver } from "@blitzjs/rpc"
import db from "db"
export default resolver.pipe(
  resolver.authorize(),
  async ({ where, orderBy, skip = 0, take = 100 }) => {
    where = {
      ...where,
      is_customer: false,
    }
    // TODO: in multi-tenant app, you must add validation to ensure correct tenant
    const {
      items: purchases,
      hasMore,
      nextPage,
      count,
    } = await paginate({
      skip,
      take,
      count: () =>
        db.order.count({
          where,
        }),
      query: (paginateArgs) =>
        db.order.findMany({
          ...paginateArgs,
          where,
          orderBy,
        }),
    })
    return {
      purchases,
      nextPage,
      hasMore,
      count,
    }
  }
)
