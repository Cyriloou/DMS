import { resolver } from "@blitzjs/rpc"
import db from "db"
import { ORDER_STATUS } from "src/utils/constant"
import { getYear, isSameMonth, isThisMonth, isThisYear } from "date-fns"

export default resolver.pipe(resolver.authorize(), async () => {
  // TODO: in multi-tenant app, you must add validation to ensure correct tenant
  const woods = await db.wood.findMany({})
  const orders = await db.order.findMany({
    where: {
      status: {
        in: [ORDER_STATUS.DELIVERED, ORDER_STATUS.PAID],
      },
    },
  })
  const clientOrders = orders.filter((el) => el.is_customer)
  const purchaseOrders = orders.filter((el) => !el.is_customer)
  return (woods || []).map((w) => {
    const clientOrderPerWood = (clientOrders || []).filter((el) => el.woodId === w.id)
    const purchaseOrderPerWood = (purchaseOrders || []).filter((el) => el.woodId === w.id)

    // Client orders
    const totalSaleQty = clientOrderPerWood.reduce((acc, val) => acc + val.quantity, 0)
    const totalSaleGain = clientOrderPerWood.reduce((acc, val) => acc + val.price, 0)
    const yearClientOrderPerWood = clientOrderPerWood.filter((el) => isThisYear(el.date))
    const yearSaleQty = yearClientOrderPerWood.reduce((acc, val) => acc + val.quantity, 0)
    const yearSaleGain = yearClientOrderPerWood.reduce((acc, val) => acc + val.price, 0)
    const monthClientOrderPerWood = clientOrderPerWood.filter((el) => isThisMonth(el.date))
    const monthSaleQty = monthClientOrderPerWood.reduce((acc, val) => acc + val.quantity, 0)
    const monthSaleGain = monthClientOrderPerWood.reduce((acc, val) => acc + val.price, 0)

    // Purhase orders
    const totalPurchaseQty = purchaseOrderPerWood.reduce((acc, val) => acc + val.quantity, 0)
    const totalPurchaseCost = purchaseOrderPerWood.reduce((acc, val) => acc + val.price, 0)
    const yearPurchaseOrderPerWood = purchaseOrderPerWood.filter((el) => isThisYear(el.date))
    const yearPurchaseQty = yearPurchaseOrderPerWood.reduce((acc, val) => acc + val.quantity, 0)
    const yearPurchaseCost = yearPurchaseOrderPerWood.reduce((acc, val) => acc + val.price, 0)
    const monthPurchaseOrderPerWood = purchaseOrderPerWood.filter((el) => isThisMonth(el.date))
    const monthPurchaseQty = monthPurchaseOrderPerWood.reduce((acc, val) => acc + val.quantity, 0)
    const monthPurchaseCost = monthPurchaseOrderPerWood.reduce((acc, val) => acc + val.price, 0)

    const currentStock = totalPurchaseQty - totalSaleQty
    const avgPurchasePrice = totalPurchaseQty > 0 ? totalPurchaseCost / totalPurchaseQty : 0
    const avgSalePrice = totalSaleQty > 0 ? totalSaleGain / totalSaleQty : 0

    const profits = totalPurchaseCost - totalSaleGain
    const avgProfits =
      totalSaleQty + totalPurchaseQty > 0 ? profits / (totalSaleQty + totalPurchaseQty) : 0

    return {
      id: w.id,
      name: w.name,
      currentStock,
      totalPurchase: purchaseOrderPerWood.length,
      totalSale: clientOrderPerWood.length,
      avgPurchasePrice,
      avgSalePrice,
      profits,
      avgProfits,

      purchaseQty: {
        total: totalPurchaseQty,
        year: yearPurchaseQty,
        month: monthPurchaseQty,
        perMonth: dataToMonthlyValues(clientOrderPerWood, "quantity"),
      },
      purchaseCost: {
        total: totalPurchaseCost,
        year: yearPurchaseCost,
        month: monthPurchaseCost,
        perMonth: dataToMonthlyValues(clientOrderPerWood, "price"),
      },
      saleQty: {
        total: totalSaleQty,
        year: yearSaleQty,
        month: monthSaleQty,
        perMonth: dataToMonthlyValues(clientOrderPerWood, "quantity"),
      },
      saleGain: {
        total: totalSaleGain,
        year: yearSaleGain,
        month: monthSaleGain,
        perMonth: dataToMonthlyValues(clientOrderPerWood, "price"),
      },
    }
  })
})

const dataToMonthlyValues = (val, key = "price") => {
  return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map((month) => {
    val
      .filter((el) => {
        const date = new Date(el.date)
        return isSameMonth(new Date(getYear(date), month, 2), date)
      })
      .reduce((acc, val) => acc + val[key], 0)
  })
}
