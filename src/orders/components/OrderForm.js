import { Grid } from "@mui/material"
import { Form } from "src/core/components/Form"
import LabeledSelectField from "src/core/components/LabeledSelectField"
import { LabeledTextField } from "src/core/components/LabeledTextField"
import AuthCardWrapper from "src/pages/auth/AuthCardWrapper"
import { orderStatusOptions } from "src/utils/constant"
export { FORM_ERROR } from "src/core/components/Form"

export function OrderForm({
  isCreated,
  woodOptions,
  supplierOptions,
  clientOptions,
  isClient,
  stocks,
  ...props
}) {
  const handlChange = (event) => {
    const {
      target: { value, name },
    } = event
    console.log("{name, value}", { name, value })
    if (name === "woodId") {
      console.log(
        "stocks.find(el => el.id ===stocks",
        stocks.find((el) => el.id === value)
      )
    }
  }
  return (
    <AuthCardWrapper>
      <Form {...props} onChange={handlChange}>
        <Grid item xs={12} sm={6}>
          <LabeledSelectField
            name="status"
            label="Status"
            placeholder="Status"
            options={orderStatusOptions}
            // readOnly
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <LabeledSelectField
            name={`clientId`}
            label={isClient ? "Client" : "Supplier"}
            placeholder={isClient ? "Client" : "Supplier"}
            options={isClient ? [...clientOptions] : [...supplierOptions]}
            readOnly={!isCreated}
          />
        </Grid>
        <Grid item xs={12}>
          <LabeledSelectField
            name={`woodId`}
            label="Wood"
            placeholder="Wood"
            options={[...woodOptions]}
            readOnly={!isCreated}
          />
        </Grid>
        <Grid item xs={12}>
          <LabeledTextField
            name={`price`}
            label="Price"
            placeholder="Price"
            readOnly={!isCreated}
            type="number"
            registerOptions={{ valueAsNumber: true }}
          />
        </Grid>
        <Grid item xs={12}>
          <LabeledTextField
            name={`quantity`}
            label="Quantity"
            placeholder="Quantity"
            readOnly={!isCreated}
            type="number"
            registerOptions={{ valueAsNumber: true }}
          />
        </Grid>
      </Form>
    </AuthCardWrapper>
  )
}
