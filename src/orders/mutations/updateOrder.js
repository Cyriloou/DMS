import { resolver } from "@blitzjs/rpc"
import db from "db"
import { UpdateOrder } from "src/auth/validations"

export default resolver.pipe(
  resolver.zod(UpdateOrder),
  resolver.authorize(),
  async ({ id, ...data }) => {
    // TODO: in multi-tenant app, you must add validation to ensure correct tenant
    const order = await db.order.update({
      where: {
        id,
      },
      data,
    })
    return order
  }
)
