import { resolver } from "@blitzjs/rpc"
import db from "db"
import { CreateOrder } from "src/auth/validations"

export default resolver.pipe(resolver.zod(CreateOrder), resolver.authorize(), async (input) => {
  // TODO: in multi-tenant app, you must add validation to ensure correct tenant
  const order = await db.order.create({
    data: input,
  })
  return order
})
