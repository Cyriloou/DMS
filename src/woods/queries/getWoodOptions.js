import { resolver } from "@blitzjs/rpc"
import db from "db"

export default resolver.pipe(resolver.authorize(), async () => {
  // TODO: in multi-tenant app, you must add validation to ensure correct tenant
  const woods = await db.wood.findMany({})
  if (!woods) throw new NotFoundError()
  return woods.map((w) => ({
    value: w.id,
    label: w.name,
  }))
})
