import { paginate } from "blitz"
import { resolver } from "@blitzjs/rpc"
import db from "db"
export default resolver.pipe(
  resolver.authorize(),
  async ({ where, orderBy, skip = 0, take = 100 }) => {
    // TODO: in multi-tenant app, you must add validation to ensure correct tenant
    const {
      items: woods,
      hasMore,
      nextPage,
      count,
    } = await paginate({
      skip,
      take,
      count: () =>
        db.wood.count({
          where,
        }),
      query: (paginateArgs) =>
        db.wood.findMany({
          ...paginateArgs,
          where,
          orderBy,
        }),
    })
    return {
      woods,
      nextPage,
      hasMore,
      count,
    }
  }
)
