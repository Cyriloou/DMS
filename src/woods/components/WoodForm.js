import { Grid } from "@mui/material"
import { Form } from "src/core/components/Form"
import { LabeledTextField } from "src/core/components/LabeledTextField"
import AuthCardWrapper from "src/pages/auth/AuthCardWrapper"
export { FORM_ERROR } from "src/core/components/Form"
export function WoodForm(props) {
  return (
    <AuthCardWrapper>
      <Form {...props}>
        <Grid item xs={12}>
          <LabeledTextField name="name" label="Name" placeholder="Name" />
        </Grid>
      </Form>
    </AuthCardWrapper>
  )
}
