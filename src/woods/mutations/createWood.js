import { resolver } from "@blitzjs/rpc"
import db from "db"
import { z } from "zod"
const CreateWood = z.object({
  name: z.string(),
})
export default resolver.pipe(resolver.zod(CreateWood), resolver.authorize(), async (input) => {
  // TODO: in multi-tenant app, you must add validation to ensure correct tenant
  const wood = await db.wood.create({
    data: input,
  })
  return wood
})
