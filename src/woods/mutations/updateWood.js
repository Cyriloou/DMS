import { resolver } from "@blitzjs/rpc"
import db from "db"
import { z } from "zod"
const UpdateWood = z.object({
  id: z.number(),
  name: z.string(),
})
export default resolver.pipe(
  resolver.zod(UpdateWood),
  resolver.authorize(),
  async ({ id, ...data }) => {
    // TODO: in multi-tenant app, you must add validation to ensure correct tenant
    const wood = await db.wood.update({
      where: {
        id,
      },
      data,
    })
    return wood
  }
)
