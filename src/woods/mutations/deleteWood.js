import { resolver } from "@blitzjs/rpc"
import db from "db"
import { z } from "zod"
const DeleteWood = z.object({
  id: z.number(),
})
export default resolver.pipe(resolver.zod(DeleteWood), resolver.authorize(), async ({ id }) => {
  // TODO: in multi-tenant app, you must add validation to ensure correct tenant
  const wood = await db.wood.deleteMany({
    where: {
      id,
    },
  })
  return wood
})
