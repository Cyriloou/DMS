export const USER_ROLES = {
  USER: "USER",
  ADMIN: "ADMIN",
  TRADER: "TRADER",
  COMMERCIAL: "COMMERCIAL",
}

export const userRoleOptions = Object.values(USER_ROLES).map((el) => ({
  label: el,
  value: el,
}))

export const CLIENT_ROLES = {
  CLIENT: "CLIENT",
  SUPPLIER: "SUPPLIER",
}

export const clientRoleOptions = Object.values(CLIENT_ROLES).map((el) => ({
  label: el,
  value: el,
}))
