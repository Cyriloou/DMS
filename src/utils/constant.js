// theme constant
export const gridSpacing = 3
export const drawerWidth = 260
export const appDrawerWidth = 320
export const isProduction = process.env.NODE_ENV === "production"

export const ITEMS_PER_PAGE = 100

export const regexEmail =
  /^\w{1,50}(?:[.+_-]\w{1,50}){0,50}@\w{1,50}(?:[.-]\w{1,50}){0,50}(?:\.(fr|com|org|net|test|ez|me|co.uk))$/i

export const regexPhoneNumber =
  /^(0[6-7])(?:[\s.-]{0,5})(\d{2})(?:[\s.-]{0,5})(\d{2})(?:[\s.-]{0,5})(\d{2})(?:[\s.-]{0,5})(\d{2})$/

export const ORDER_STATUS = {
  CREATED: "created",
  REFUSED: "refused",
  ACCEPTED: "accepted",
  CANCELED: "canceled",
  DELIVERED: "delivered",
  PAID: "paid",
}

export const orderStatusOptions = Object.values(ORDER_STATUS).map((el) => ({
  label: el,
  value: el,
}))

export const localStorageName = process.env.NEXT_PUBLIC_LOCAL_STORAGE_NAME
export const isFormDebug =
  process.env.NEXT_PUBLIC_DEBUG_FORM && process.env.NEXT_PUBLIC_DEBUG_FORM === "true"
