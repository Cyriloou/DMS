export function currencyFormat(num) {
  return "€" + num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ")
}

export const myLoader = ({ src, width, quality }) => {
  return `https://example.com/${src}?w=${width}&q=${quality || 75}`
}
