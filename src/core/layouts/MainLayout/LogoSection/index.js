// material-ui
import { ButtonBase } from "@mui/material"

// project imports
import { useLocalStorage } from "src/utils/hooks/useLocalStorage"
import config from "src/config"
import Logo from "src/ui-component/Logo"
import { useRouter } from "next/router"
import { localStorageName } from "src/utils/constant"

// ==============================|| MAIN LOGO ||============================== //

const LogoSection = () => {
  const router = useRouter()
  const [localStorage, setLocalStorage] = useLocalStorage(localStorageName)
  const defaultId = localStorage?.customization?.defaultId || "default"
  const handleClick = (event) => {
    event.preventDefault()
    setLocalStorage({ ...localStorage, isOpen: [defaultId] })
    router.push(config.defaultPath)
  }

  return (
    <ButtonBase disableRipple onClick={handleClick}>
      <Logo />
    </ButtonBase>
  )
}

export default LogoSection
