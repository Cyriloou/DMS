// assets
import { Routes } from "@blitzjs/next"
import PeopleIcon from "@mui/icons-material/People"

// constant
const icons = {
  PeopleIcon,
}

// ==============================|| UTILITIES MENU ITEMS ||============================== //

const clients = {
  id: "clients",
  title: "Clients",
  type: "group",
  children: [
    {
      id: "util-clients",
      title: "Clients",
      type: "item",
      url: Routes.ClientsPage(),
      icon: icons.PeopleIcon,
      breadcrumbs: false,
    },
  ],
}

export default clients
