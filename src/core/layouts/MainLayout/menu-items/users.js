// assets
import { Routes } from "@blitzjs/next"
import PeopleOutlineIcon from "@mui/icons-material/PeopleOutline"

// constant
const icons = {
  PeopleOutlineIcon,
}

// ==============================|| UTILITIES MENU ITEMS ||============================== //

const users = {
  id: "users",
  title: "Users",
  type: "group",
  children: [
    {
      id: "util-users",
      title: "Users",
      type: "item",
      url: Routes.UsersPage(),
      icon: icons.PeopleOutlineIcon,
      breadcrumbs: false,
    },
  ],
}

export default users
