// assets
import { Routes } from "@blitzjs/next"
import ShowChartIcon from "@mui/icons-material/ShowChart"

// constant
const icons = {
  ShowChartIcon,
}

// ==============================|| UTILITIES MENU ITEMS ||============================== //

const stocks = {
  id: "stocks",
  title: "Stocks",
  type: "group",
  children: [
    {
      id: "util-stock",
      title: "Stock",
      type: "item",
      url: Routes.StocksPage(),
      icon: icons.ShowChartIcon,
      breadcrumbs: false,
    },
  ],
}

export default stocks
