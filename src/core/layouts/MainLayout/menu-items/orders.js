// assets
import { Routes } from "@blitzjs/next"
import ShoppingBasketIcon from "@mui/icons-material/ShoppingBasket"

// constant
const icons = {
  ShoppingBasketIcon,
}

// ==============================|| UTILITIES MENU ITEMS ||============================== //

const orders = {
  id: "orders",
  title: "Orders",
  type: "group",
  children: [
    {
      id: "util-orders",
      title: "Orders",
      type: "item",
      url: Routes.OrdersPage(),
      icon: icons.ShoppingBasketIcon,
      breadcrumbs: false,
    },
  ],
}

export default orders
