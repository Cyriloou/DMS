// assets
import { Routes } from "@blitzjs/next"
import BusinessIcon from "@mui/icons-material/Business"

// constant
const icons = {
  BusinessIcon,
}

// ==============================|| UTILITIES MENU ITEMS ||============================== //

const suppliers = {
  id: "suppliers",
  title: "Suppliers",
  type: "group",
  children: [
    {
      id: "util-suppliers",
      title: "Suppliers",
      type: "item",
      url: Routes.SuppliersPage(),
      icon: icons.BusinessIcon,
      breadcrumbs: false,
    },
  ],
}

export default suppliers
