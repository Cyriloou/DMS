import clients from "./clients"
// import orders from "./orders"
import suppliers from "./suppliers"
import { USER_ROLES } from "src/utils/enums"
import users from "./users"
import orders from "./orders"
import purchases from "./purchases"
import stocks from "./stocks"
// import woods from "./woods"

// ==============================|| MENU ITEMS ||============================== //

const menuItems = {
  items: [
    {
      item: users,
      allowedRoles: [USER_ROLES.ADMIN, USER_ROLES.TRADER],
    },
    {
      item: clients,
      allowedRoles: [USER_ROLES.ADMIN, USER_ROLES.TRADER],
    },
    {
      item: suppliers,
      allowedRoles: [USER_ROLES.ADMIN, USER_ROLES.TRADER, USER_ROLES.COMMERCIAL],
    },
    {
      item: orders,
      allowedRoles: [USER_ROLES.ADMIN, USER_ROLES.TRADER, USER_ROLES.COMMERCIAL],
    },
    {
      item: purchases,
      allowedRoles: [USER_ROLES.ADMIN, USER_ROLES.TRADER, USER_ROLES.COMMERCIAL],
    },
    {
      item: stocks,
      allowedRoles: [USER_ROLES.ADMIN, USER_ROLES.TRADER, USER_ROLES.COMMERCIAL],
    },
  ],
}

export default menuItems
