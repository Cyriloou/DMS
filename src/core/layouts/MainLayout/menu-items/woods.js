// assets
import { Routes } from "@blitzjs/next"
import ForestIcon from "@mui/icons-material/Forest"

// constant
const icons = {
  ForestIcon,
}

// ==============================|| UTILITIES MENU ITEMS ||============================== //

const woods = {
  id: "woods",
  title: "Woods",
  type: "group",
  children: [
    {
      id: "util-woods",
      title: "Woods",
      type: "item",
      url: Routes.WoodsPage(),
      icon: icons.ForestIcon,
      breadcrumbs: false,
    },
  ],
}

export default woods
