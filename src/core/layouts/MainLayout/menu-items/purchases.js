// assets
import { Routes } from "@blitzjs/next"
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart"

// constant
const icons = {
  ShoppingCartIcon,
}

// ==============================|| UTILITIES MENU ITEMS ||============================== //

const purchases = {
  id: "purchases",
  title: "Purchases",
  type: "group",
  children: [
    {
      id: "util-purchase",
      title: "Purchase",
      type: "item",
      url: Routes.PurchasesPage(),
      icon: icons.ShoppingCartIcon,
      breadcrumbs: false,
    },
  ],
}

export default purchases
