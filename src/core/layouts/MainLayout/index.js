import { AppBar, Box, CssBaseline, Toolbar, useMediaQuery } from "@mui/material"
import Head from "next/head"
import React from "react"
import { styled, useTheme } from "@mui/material/styles"
import Sidebar from "./SideBar"
import { useLocalStorage } from "src/utils/hooks/useLocalStorage"
import { drawerWidth, localStorageName } from "src/utils/constant"
import Header from "src/core/layouts/MainLayout/Header"
import { useCurrentUser } from "src/users/hooks/useCurrentUser"
import Layout from "../Layout"

// styles
const Main = styled("main", { shouldForwardProp: (prop) => prop !== "open" })(
  ({ theme, open }) => ({
    ...theme.typography.mainContent,
    ...(!open && {
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 0,
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      [theme.breakpoints.up("md")]: {
        marginLeft: -(drawerWidth - 20),
        width: `calc(100% - ${drawerWidth}px)`,
      },
      [theme.breakpoints.down("md")]: {
        marginLeft: "20px",
        width: `calc(100% - ${drawerWidth}px)`,
        padding: "16px",
      },
      [theme.breakpoints.down("sm")]: {
        marginLeft: "10px",
        width: `calc(100% - ${drawerWidth}px)`,
        padding: "16px",
        marginRight: "10px",
      },
    }),
    ...(open && {
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
      borderBottomLeftRadius: 0,
      borderBottomRightRadius: 0,
      width: `calc(100% - ${drawerWidth}px)`,
      [theme.breakpoints.down("md")]: {
        marginLeft: "20px",
      },
      [theme.breakpoints.down("sm")]: {
        marginLeft: "10px",
      },
    }),
  })
)

const MainLayout = ({ title, children }) => {
  const currentUser = useCurrentUser()
  const theme = useTheme()
  const matchDownMd = useMediaQuery(theme.breakpoints.down("md"))
  // Handle left drawer
  const [localStorage, setLocalStorage] = useLocalStorage(localStorageName)
  const leftDrawerOpened = localStorage?.customization?.opened || false

  const handleLeftDrawerToggle = () => {
    setLocalStorage({
      ...localStorage,
      customization: {
        ...(localStorage?.customization || {}),
        opened: !leftDrawerOpened,
      },
    })
  }
  if (!currentUser) {
    return <Layout>{children}</Layout>
  }
  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <Head>
        <title>{title || "dms-test"}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <>
        {/* header */}
        <AppBar
          enableColorOnDark
          position="fixed"
          color="inherit"
          elevation={0}
          sx={{
            bgcolor: theme.palette.background.default,
            transition: leftDrawerOpened ? theme.transitions.create("width") : "none",
          }}
        >
          <Toolbar>
            <Header handleLeftDrawerToggle={handleLeftDrawerToggle} />
          </Toolbar>
        </AppBar>

        {/* drawer */}
        <Sidebar
          drawerOpen={!matchDownMd ? leftDrawerOpened : !leftDrawerOpened}
          drawerToggle={handleLeftDrawerToggle}
        />
      </>
      <Main theme={theme} open={leftDrawerOpened}>
        {children}
      </Main>
    </Box>
  )
}
export default MainLayout
