import { Grid } from "@mui/material"
import React from "react"

export default function FormLayout({ children }) {
  return (
    <Grid container direction="column" justifyContent="flex-end" sx={{ minHeight: "100vh" }}>
      <Grid item xs={12}>
        <Grid
          container
          justifyContent="center"
          alignItems="center"
          sx={{ minHeight: "calc(100vh - 68px)" }}
        >
          <Grid item sx={{ m: { xs: 1, sm: 3 }, mb: 0 }}>
            {children}
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} sx={{ m: 3, mt: 1 }}></Grid>
    </Grid>
  )
}
