import { forwardRef } from "react"
import { useFormContext } from "react-hook-form"
import { ErrorMessage } from "@hookform/error-message"
import { FormControl, IconButton, InputLabel, MenuItem, Select } from "@mui/material"
import DeleteIcon from "@mui/icons-material/Delete"

const LabeledSelectField = ({
  label,
  outerProps,
  labelProps,
  name,
  options,
  disabled,
  registerOptions,
}) => {
  const {
    register,
    reset,
    formState: { isSubmitting, errors },
  } = useFormContext()
  return (
    <>
      <FormControl fullWidth {...outerProps}>
        <InputLabel id={`${name}-select-label`} {...labelProps}>
          {label}
        </InputLabel>
        <Select
          labelId={`${name}-select-label`}
          id={`${name}-select`}
          disabled={isSubmitting || disabled}
          // inputProps={{
          //   inputRef: (ref) => {
          //     if (!ref) return
          //     register(name, {
          //       ...registerOptions,
          //       value: ref.value,
          //     })
          //   },
          // }}
          {...register(name, registerOptions)}
          endAdornment={
            <IconButton
              aria-label="delete"
              size="large"
              type="button"
              onClick={() => {
                reset((formValues) => ({
                  ...formValues,
                  [name]: null,
                }))
              }}
              style={{
                marginLeft: "0.5rem",
              }}
              variant="outlined"
              color="error"
              disabled={disabled}
            >
              <DeleteIcon fontSize="inherit" />
            </IconButton>
          }
        >
          {(options || []).map((o) => (
            <MenuItem value={o.value} key={o.value}>
              {o.label}
            </MenuItem>
          ))}
        </Select>
        <ErrorMessage
          render={({ message }) => (
            <div
              role="alert"
              style={{
                color: "red",
              }}
            >
              {message}
            </div>
          )}
          errors={errors}
          name={name}
        />

        <style jsx>{`
          label {
            display: flex;
            flex-direction: column;
            align-items: start;
            font-size: 1rem;
          }
          input {
            font-size: 1rem;
            padding: 0.25rem 0.5rem;
            border-radius: 3px;
            border: 1px solid purple;
            appearance: none;
            margin-top: 0.5rem;
          }
        `}</style>
      </FormControl>
    </>
  )
}

export default LabeledSelectField
export { LabeledSelectField }
