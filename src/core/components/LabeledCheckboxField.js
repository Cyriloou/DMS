import { forwardRef } from "react"
import { useFormContext } from "react-hook-form"
import { ErrorMessage } from "@hookform/error-message"
import { Checkbox, FormControlLabel } from "@mui/material"

const LabeledCheckboxField = forwardRef(
  ({ label, outerProps, labelProps, name, options, disabled, ...props }, ref) => {
    const {
      register,
      formState: { isSubmitting, errors },
    } = useFormContext()

    return (
      <>
        <FormControlLabel
          control={
            <Checkbox
              id={`${name}-checkbox`}
              {...register(name)}
              {...props}
              disabled={isSubmitting || disabled}
            />
          }
          label={label}
        />
        <ErrorMessage
          render={({ message }) => (
            <div
              role="alert"
              style={{
                color: "red",
              }}
            >
              {message}
            </div>
          )}
          errors={errors}
          name={name}
        />
        <style jsx>{`
          label {
            display: flex;
            flex-direction: column;
            align-items: start;
            font-size: 1rem;
          }
          input {
            font-size: 1rem;
            padding: 0.25rem 0.5rem;
            border-radius: 3px;
            border: 1px solid purple;
            appearance: none;
            margin-top: 0.5rem;
          }
        `}</style>
      </>
    )
  }
)

export default LabeledCheckboxField
export { LabeledCheckboxField }
