import { useEffect, useMemo, useState } from "react"
import { FormProvider, useForm } from "react-hook-form"
import { zodResolver } from "@hookform/resolvers/zod"
import { Box, Button } from "@mui/material"
import AnimateButton from "src/ui-component/extended/AnimateButton"
import { isFormDebug } from "src/utils/constant"
import _ from "lodash"
export const FORM_ERROR = "FORM_ERROR"

export function Form({ children, submitText, schema, initialValues, onSubmit, ...props }) {
  const ctx = useForm({
    mode: "onBlur",
    resolver: schema ? zodResolver(schema) : undefined,
    defaultValues: useMemo(
      () => ({
        ...(initialValues || {}),
      }),
      [initialValues]
    ),
  })
  const {
    formState: { errors },
  } = ctx

  useEffect(() => {
    if (!_.isEmpty(errors) && isFormDebug) {
      console.error(errors)
    }
  }, [errors])

  const [formError, setFormError] = useState(null)
  return (
    <FormProvider {...ctx}>
      <form
        onSubmit={ctx.handleSubmit(async (values) => {
          const result = (await onSubmit(values)) || {}
          for (const [key, value] of Object.entries(result)) {
            if (key === FORM_ERROR) {
              setFormError(value)
            } else {
              ctx.setError(key, {
                type: "submit",
                message: value,
              })
            }
          }
        })}
        className="form"
        {...props}
      >
        {/* Form fields supplied as children are rendered here */}
        {children}

        {formError && (
          <div
            role="alert"
            style={{
              color: "red",
            }}
          >
            {formError}
          </div>
        )}

        {submitText && (
          <Box sx={{ mt: 2 }}>
            <AnimateButton>
              <Button
                disableElevation
                disabled={ctx.formState.isSubmitting}
                fullWidth
                size="large"
                type="submit"
                variant="contained"
                color="secondary"
              >
                {submitText}
              </Button>
            </AnimateButton>
          </Box>
        )}

        <style global jsx>{`
          .form > * + * {
            margin-top: 1rem;
          }
        `}</style>
      </form>
      {isFormDebug && <pre>{JSON.stringify(ctx.watch(), null, 2)}</pre>}
    </FormProvider>
  )
}
export default Form
