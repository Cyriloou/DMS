import { forwardRef } from "react"
import { useFormContext } from "react-hook-form"
import { ErrorMessage } from "@hookform/error-message"
import { FormControl, Input, InputLabel } from "@mui/material"

export const LabeledTextField = forwardRef(
  ({ label, outerProps, labelProps, name, registerOptions, ...props }, ref) => {
    const {
      register,
      formState: { isSubmitting, errors },
    } = useFormContext()
    return (
      <FormControl fullWidth {...outerProps}>
        <InputLabel id={`${name}-select-label`} {...labelProps}>
          {label}
        </InputLabel>
        <Input disabled={isSubmitting} {...register(name, registerOptions)} {...props} />

        <ErrorMessage
          render={({ message }) => (
            <div
              role="alert"
              style={{
                color: "red",
              }}
            >
              {message}
            </div>
          )}
          errors={errors}
          name={name}
        />

        <style jsx>{`
          label {
            display: flex;
            flex-direction: column;
            align-items: start;
            font-size: 1rem;
          }
          input {
            font-size: 1rem;
            padding: 0.25rem 0.5rem;
            border-radius: 3px;
            border: 1px solid purple;
            appearance: none;
            margin-top: 0.5rem;
          }
        `}</style>
      </FormControl>
    )
  }
)
export default LabeledTextField
