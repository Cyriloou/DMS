import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import { useRouter } from "next/router"
import { useQuery, useMutation } from "@blitzjs/rpc"
import { useParam } from "@blitzjs/next"
import Layout from "src/core/layouts/Layout"
import getOrder from "src/orders/queries/getOrder"
import updateOrder from "src/orders/mutations/updateOrder"
import { OrderForm, FORM_ERROR } from "src/orders/components/OrderForm"
import PrevBtn from "src/ui-component/buttons/PrevBtn"
import getWoodOptions from "src/woods/queries/getWoodOptions"
import getClientOptions from "src/clients/queries/getClientOptions"
import getSupplierOptions from "src/clients/queries/getSupplierOptions"
import { USER_ROLES } from "src/utils/enums"
import { UpdatePurchase } from "src/auth/validations"

export const EditPurchase = () => {
  const router = useRouter()
  const purchaseId = useParam("purchaseId", "number")
  const [order, { setQueryData }] = useQuery(
    getOrder,
    {
      id: purchaseId,
    },
    {
      // This ensures the query never refreshes and overwrites the form data while the user is editing.
      staleTime: Infinity,
    }
  )
  const purchase = order
  const [updateOrderMutation] = useMutation(updateOrder)
  const [woodOptions] = useQuery(getWoodOptions, {})
  const [supplierOptions] = useQuery(getSupplierOptions, {})
  const [clientOptions] = useQuery(getClientOptions, {})
  return (
    <>
      <Head>
        <title>Edit Purchase {purchase.id}</title>
      </Head>

      <div>
        <h1>Edit Purchase {purchase.id}</h1>
        <pre>{JSON.stringify(purchase, null, 2)}</pre>

        <OrderForm
          submitText="Update Purchase"
          // TODO use a zod schema for form validation
          //  - Tip: extract mutation's schema into a shared `validations.ts` file and
          //         then import and use it here
          schema={UpdatePurchase}
          initialValues={purchase}
          woodOptions={woodOptions}
          supplierOptions={supplierOptions}
          clientOptions={clientOptions}
          onSubmit={async (values) => {
            try {
              const updated = await updateOrderMutation({
                id: purchase.id,
                ...values,
              })
              await setQueryData(updated)
              await router.push(
                Routes.ShowPurchasePage({
                  purchaseId: updated.id,
                })
              )
            } catch (error) {
              console.error(error)
              return {
                [FORM_ERROR]: error.toString(),
              }
            }
          }}
        />
      </div>
    </>
  )
}
const EditPurchasePage = () => {
  return (
    <div>
      <Suspense fallback={<div>Loading...</div>}>
        <EditPurchase />
      </Suspense>

      <PrevBtn href={Routes.OrdersPage()}>Orders</PrevBtn>
    </div>
  )
}
EditPurchasePage.authenticate = {
  role: [USER_ROLES.ADMIN, USER_ROLES.TRADER, USER_ROLES.COMMERCIAL],
}
EditPurchasePage.getLayout = (page) => <Layout>{page}</Layout>
export default EditPurchasePage
