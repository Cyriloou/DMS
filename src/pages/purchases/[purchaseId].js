import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import { useRouter } from "next/router"
import { useQuery, useMutation } from "@blitzjs/rpc"
import { useParam } from "@blitzjs/next"
import Layout from "src/core/layouts/Layout"
import PrevBtn from "src/ui-component/buttons/PrevBtn"
import EditBtn from "src/ui-component/buttons/EditBtn"
import DeleteBtn from "src/ui-component/buttons/DeleteBtn"
import deleteOrder from "src/orders/mutations/deleteOrder"
import { USER_ROLES } from "src/utils/enums"
import getPurchase from "src/orders/queries/getPurchase"

export const Purchase = () => {
  const router = useRouter()
  const purchaseId = useParam("purchaseId", "number")
  const [deletePurchaseMutation] = useMutation(deleteOrder)
  const [purchase] = useQuery(getPurchase, {
    id: purchaseId,
  })
  return (
    <>
      <Head>
        <title>Purchase {purchase.id}</title>
      </Head>

      <div>
        <h1>Purchase {purchase.id}</h1>

        <EditBtn
          href={Routes.EditPurchasePage({
            purchaseId: purchase.id,
          })}
        />

        <DeleteBtn
          onClick={async () => {
            if (window.confirm("This will be deleted")) {
              await deletePurchaseMutation({
                id: purchase.id,
              })
              await router.push(Routes.PurchasesPage())
            }
          }}
        />
      </div>
    </>
  )
}
const ShowPurchasePage = () => {
  return (
    <div>
      <PrevBtn href={Routes.PurchasesPage()}>Purchases</PrevBtn>

      <Suspense fallback={<div>Loading...</div>}>
        <Purchase />
      </Suspense>
    </div>
  )
}
ShowPurchasePage.authenticate = {
  role: [USER_ROLES.ADMIN, USER_ROLES.TRADER, USER_ROLES.COMMERCIAL],
}
ShowPurchasePage.getLayout = (page) => <Layout>{page}</Layout>
export default ShowPurchasePage
