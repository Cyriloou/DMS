import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { usePaginatedQuery } from "@blitzjs/rpc"
import { useRouter } from "next/router"
import Layout from "src/core/layouts/Layout"
import ListPrevNextBtn from "src/ui-component/list/ListPrevNextBtn"
import DefaultBox from "src/ui-component/DefaultBox"
import Btn from "src/ui-component/buttons/Btn"
import MainCard from "src/ui-component/cards/MainCard"
import { Card } from "@mui/material"
import getPurchases from "src/orders/queries/getPurchases"
import _ from "lodash"
const ITEMS_PER_PAGE = 100
export const PurchasesList = () => {
  const router = useRouter()
  const page = Number(router.query.page) || 0
  const [{ purchases, hasMore }] = usePaginatedQuery(getPurchases, {
    orderBy: {
      id: "asc",
    },
    skip: ITEMS_PER_PAGE * page,
    take: ITEMS_PER_PAGE,
  })
  return (
    <>
      <MainCard title="Order">
        {purchases.map((purchase) => (
          <Card sx={{ overflow: "hidden" }} key={purchase.id}>
            <Link
              href={Routes.ShowPurchasePage({
                purchaseId: purchase.id,
              })}
            >
              <DefaultBox bgcolor="primary.light" dark data={_.pick(purchase, ["id", "name"])} />
            </Link>
          </Card>
        ))}
      </MainCard>
      <ListPrevNextBtn page={page} disabledPrev={page === 0} disabledNext={!hasMore} />
    </>
  )
}
const PurchasesPage = () => {
  return (
    <Layout>
      <Head>
        <title>Purchases</title>
      </Head>

      <div>
        <Btn href={Routes.NewPurchasePage()}>Create Purchase</Btn>

        <Suspense fallback={<div>Loading...</div>}>
          <PurchasesList />
        </Suspense>
      </div>
    </Layout>
  )
}
export default PurchasesPage
