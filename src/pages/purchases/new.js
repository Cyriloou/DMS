import { Routes } from "@blitzjs/next"
import { useRouter } from "next/router"
import { useMutation, useQuery } from "@blitzjs/rpc"
import Layout from "src/core/layouts/Layout"
import getSupplierOptions from "src/clients/queries/getSupplierOptions"
import getWoodOptions from "src/woods/queries/getWoodOptions"
import PrevBtn from "src/ui-component/buttons/PrevBtn"
import { OrderForm, FORM_ERROR } from "src/orders/components/OrderForm"
import createOrder from "src/orders/mutations/createOrder"
import { USER_ROLES } from "src/utils/enums"
import { CreatePurchase } from "src/auth/validations"
import { ORDER_STATUS } from "src/utils/constant"

const NewPurchasePage = () => {
  const router = useRouter()
  const [createPurchaseMutation] = useMutation(createOrder)
  const [woodOptions] = useQuery(getWoodOptions, {})
  const [supplierOptions] = useQuery(getSupplierOptions, {})
  return (
    <Layout title={"Create New Purchase"}>
      <h1>Create New Purchase</h1>

      <OrderForm
        submitText="Create Purchase"
        // TODO use a zod schema for form validation
        //  - Tip: extract mutation's schema into a shared `validations.ts` file and
        //         then import and use it here
        schema={CreatePurchase}
        initialValues={{ is_customer: true, status: ORDER_STATUS.CREATED }}
        woodOptions={woodOptions}
        supplierOptions={supplierOptions}
        isCreated
        onSubmit={async (values) => {
          values.is_customer = false
          try {
            const purchase = await createPurchaseMutation(values)
            await router.push(
              Routes.ShowPurchasePage({
                purchaseId: purchase.id,
              })
            )
          } catch (error) {
            console.error(error)
            return {
              [FORM_ERROR]: error.toString(),
            }
          }
        }}
      />

      <PrevBtn href={Routes.PurchasesPage()}>Purchases</PrevBtn>
    </Layout>
  )
}
NewPurchasePage.authenticate = {
  role: [USER_ROLES.ADMIN, USER_ROLES.TRADER, USER_ROLES.COMMERCIAL],
}
export default NewPurchasePage
