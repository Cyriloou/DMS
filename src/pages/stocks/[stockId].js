import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import { useParam } from "@blitzjs/next"
import PrevBtn from "src/ui-component/buttons/PrevBtn"
import getStocks from "src/orders/queries/getStocks"
import { useQuery } from "@blitzjs/rpc"
import { Grid } from "@mui/material"
import EarningCard from "src/ui-component/cards/EarningCard"
import { gridSpacing } from "src/utils/constant"
import TotalIncomeDarkCard from "src/ui-component/cards/TotalIncomeDarkCard"
import TotalOrderLineChartCard from "src/ui-component/cards/TotalOrderLineChartCard"
import TotalIncomeLightCard from "src/ui-component/cards/TotalIncomeLightCard"

export const Stock = () => {
  const stockId = useParam("stockId", "number")
  const [stocks, { isLoading }] = useQuery(getStocks)
  const stock = stocks.find((el) => el?.id && `${el.id}` === `${stockId}`)
  return (
    <>
      <Head>
        <title>Stock {stock.id}</title>
      </Head>

      <div>
        <h1>Stock {stock.name}</h1>
        <Grid container spacing={gridSpacing}>
          <Grid item xs={12}>
            <Grid container spacing={gridSpacing}>
              <Grid item lg={4} md={6} sm={6} xs={12}>
                <Suspense fallback="Loading...">
                  <EarningCard
                    isLoading={isLoading}
                    value={stock.profits}
                    title={"Total Earning"}
                    disabledMenu
                  />
                </Suspense>
              </Grid>
              <Grid item lg={4} md={6} sm={6} xs={12}>
                <TotalOrderLineChartCard isLoading={isLoading} value={stock.saleQty} />
              </Grid>
              <Grid item lg={4} md={12} sm={12} xs={12}>
                <Grid container spacing={gridSpacing}>
                  <Grid item sm={6} xs={12} md={6} lg={12}>
                    <TotalIncomeDarkCard isLoading={isLoading} value={stock.profits} />
                  </Grid>
                  <Grid item sm={6} xs={12} md={6} lg={12}>
                    <TotalIncomeLightCard
                      isLoading={isLoading}
                      value={stock.purchaseCost.total}
                      title="Total Purchase"
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Grid container spacing={gridSpacing}>
              <Grid item xs={12} md={8}>
                {/* hide as ReferenceError: window is not defined */}
                {/* <TotalGrowthBarChart
                  series={[
                    {
                      name: "purchase",
                      data: stock?.purchaseQty?.perMonth,
                    },
                    {
                      name: "sales",
                      data: stock?.saleQty?.perMonth,
                    },
                  ]}
                /> */}
              </Grid>
              <Grid item xs={12} md={4}>
                {/* <PopularCard isLoading={isLoading} /> */}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </>
  )
}
const ShowStockPage = () => {
  return (
    <div>
      <PrevBtn href={Routes.StocksPage()}>Stocks</PrevBtn>

      <Suspense fallback={<div>Loading...</div>}>
        <Stock />
      </Suspense>
    </div>
  )
}
ShowStockPage.authenticate = true
export default ShowStockPage
