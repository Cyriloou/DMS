import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { useQuery } from "@blitzjs/rpc"
import Layout from "src/core/layouts/Layout"
import getStocks from "src/orders/queries/getStocks"
import ListPrevNextBtn from "src/ui-component/list/ListPrevNextBtn"
import { Card } from "@mui/material"
import DefaultBox from "src/ui-component/DefaultBox"
import MainCard from "src/ui-component/cards/MainCard"
import _ from "lodash"

export const StocksList = () => {
  const [stocks] = useQuery(getStocks)
  return (
    <>
      <MainCard title="Stock">
        {stocks.map((stock) => (
          <Card sx={{ overflow: "hidden" }} key={stocks.id}>
            <Link
              href={Routes.ShowStockPage({
                stockId: stock.id,
              })}
            >
              <DefaultBox
                bgcolor="primary.light"
                dark
                data={_.pick(stock, ["id", "name", "email"])}
              />
            </Link>
          </Card>
        ))}
      </MainCard>
      <ListPrevNextBtn />
    </>
  )
}
const StocksPage = () => {
  return (
    <Layout>
      <Head>
        <title>Stocks</title>
      </Head>

      <div>
        <Suspense fallback={<div>Loading...</div>}>
          <StocksList />
        </Suspense>
      </div>
    </Layout>
  )
}
export default StocksPage
