import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { usePaginatedQuery } from "@blitzjs/rpc"
import { useRouter } from "next/router"
import Layout from "src/core/layouts/Layout"
import getOrders from "src/orders/queries/getOrders"
import ListPrevNextBtn from "src/ui-component/list/ListPrevNextBtn"
import Btn from "src/ui-component/buttons/Btn"
import DefaultBox from "src/ui-component/DefaultBox"
import MainCard from "src/ui-component/cards/MainCard"
import { Card } from "@mui/material"
import _ from "lodash"

const ITEMS_PER_PAGE = 100

export const OrdersList = () => {
  const router = useRouter()
  const page = Number(router.query.page) || 0

  const [{ orders, hasMore }] = usePaginatedQuery(getOrders, {
    orderBy: {
      id: "asc",
    },
    skip: ITEMS_PER_PAGE * page,
    take: ITEMS_PER_PAGE,
  })
  return (
    <>
      <MainCard title="Order">
        {orders.map((order) => (
          <Card sx={{ overflow: "hidden" }} key={order.id}>
            <Link
              href={Routes.ShowOrderPage({
                orderId: order.id,
              })}
            >
              <DefaultBox bgcolor="primary.light" dark data={_.pick(order, ["id", "name"])} />
            </Link>
          </Card>
        ))}
      </MainCard>
      <ListPrevNextBtn page={page} disabledPrev={page === 0} disabledNext={!hasMore} />
    </>
  )
}
const OrdersPage = () => {
  return (
    <Layout>
      <Head>
        <title>Orders</title>
      </Head>

      <div>
        <Btn href={Routes.NewOrderPage()}>Create Order</Btn>

        <Suspense fallback={<div>Loading...</div>}>
          <OrdersList />
        </Suspense>
      </div>
    </Layout>
  )
}
export default OrdersPage
