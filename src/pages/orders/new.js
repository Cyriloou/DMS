import { Routes } from "@blitzjs/next"
import Link from "next/link"
import { useRouter } from "next/router"
import { useMutation, useQuery } from "@blitzjs/rpc"
import Layout from "src/core/layouts/Layout"
import createOrder from "src/orders/mutations/createOrder"
import { OrderForm, FORM_ERROR } from "src/orders/components/OrderForm"
import PrevBtn from "src/ui-component/buttons/PrevBtn"
import { CreateOrder } from "src/auth/validations"
import { USER_ROLES } from "src/utils/enums"
import { Suspense } from "react"
import getWoodOptions from "src/woods/queries/getWoodOptions"
import getClientOptions from "src/clients/queries/getClientOptions"
import { ORDER_STATUS } from "src/utils/constant"
import getStocks from "src/orders/queries/getStocks"
const NewOrderPage = () => {
  const router = useRouter()
  const [createOrderMutation] = useMutation(createOrder)
  const [woodOptions] = useQuery(getWoodOptions, {})
  const [clientOptions] = useQuery(getClientOptions, {})
  const [stocks] = useQuery(getStocks)
  return (
    <Suspense fallback="Loading...">
      <Layout title={"Create New Order"}>
        <h1>Create New Order</h1>

        <OrderForm
          submitText="Create Order"
          // TODO use a zod schema for form validation
          //  - Tip: extract mutation's schema into a shared `validations.ts` file and
          //         then import and use it here
          schema={CreateOrder}
          initialValues={{ is_customer: true, status: ORDER_STATUS.CREATED }}
          woodOptions={woodOptions}
          clientOptions={clientOptions}
          isCreated
          isClient
          stocks={stocks}
          onSubmit={async (values) => {
            values.is_customer = true
            try {
              const order = await createOrderMutation(values)
              await router.push(
                Routes.ShowOrderPage({
                  orderId: order.id,
                })
              )
            } catch (error) {
              console.error(error)
              return {
                [FORM_ERROR]: error.toString(),
              }
            }
          }}
        />

        <PrevBtn href={Routes.OrdersPage()}>Orders</PrevBtn>
      </Layout>
    </Suspense>
  )
}
NewOrderPage.authenticate = { role: [USER_ROLES.ADMIN, USER_ROLES.TRADER, USER_ROLES.COMMERCIAL] }
export default NewOrderPage
