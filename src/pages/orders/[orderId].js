import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { useRouter } from "next/router"
import { useQuery, useMutation } from "@blitzjs/rpc"
import { useParam } from "@blitzjs/next"
import Layout from "src/core/layouts/Layout"
import getOrder from "src/orders/queries/getOrder"
import deleteOrder from "src/orders/mutations/deleteOrder"
import EditBtn from "src/ui-component/buttons/EditBtn"
import DeleteBtn from "src/ui-component/buttons/DeleteBtn"
import PrevBtn from "src/ui-component/buttons/PrevBtn"
import { USER_ROLES } from "src/utils/enums"
export const Order = () => {
  const router = useRouter()
  const orderId = useParam("orderId", "number")
  const [deleteOrderMutation] = useMutation(deleteOrder)
  const [order] = useQuery(getOrder, {
    id: orderId,
  })
  return (
    <>
      <Head>
        <title>Order {order.id}</title>
      </Head>

      <div>
        <h1>Order {order.id}</h1>

        <EditBtn
          href={Routes.EditSupplierPage({
            supplierId: supplier.id,
          })}
        />

        <DeleteBtn
          onClick={async () => {
            if (window.confirm("This will be deleted")) {
              await deleteOrderMutation({
                id: order.id,
              })
              await router.push(Routes.OrdersPage())
            }
          }}
        />
      </div>
    </>
  )
}
const ShowOrderPage = () => {
  return (
    <div>
      <PrevBtn href={Routes.OrdersPage()}>Orders</PrevBtn>

      <Suspense fallback={<div>Loading...</div>}>
        <Order />
      </Suspense>
    </div>
  )
}
ShowOrderPage.authenticate = { role: [USER_ROLES.ADMIN, USER_ROLES.TRADER, USER_ROLES.COMMERCIAL] }
ShowOrderPage.getLayout = (page) => <Layout>{page}</Layout>
export default ShowOrderPage
