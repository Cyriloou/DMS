import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { usePaginatedQuery } from "@blitzjs/rpc"
import { useRouter } from "next/router"
import Layout from "src/core/layouts/Layout"
import getClients from "src/clients/queries/getClients"
import Btn from "src/ui-component/buttons/Btn"
import ListPrevNextBtn from "src/ui-component/list/ListPrevNextBtn"
import { Card } from "@mui/material"
import MainCard from "src/ui-component/cards/MainCard"
import DefaultBox from "src/ui-component/DefaultBox"
import _ from "lodash"
const ITEMS_PER_PAGE = 100

export const ClientsList = () => {
  const router = useRouter()
  const page = Number(router.query.page) || 0
  const [{ clients, hasMore }] = usePaginatedQuery(getClients, {
    orderBy: {
      id: "asc",
    },
    skip: ITEMS_PER_PAGE * page,
    take: ITEMS_PER_PAGE,
  })

  return (
    <>
      <MainCard title="Clients">
        {clients.map((client) => (
          <Card sx={{ overflow: "hidden" }} key={client.id}>
            <Link
              href={Routes.ShowClientPage({
                clientId: client.id,
              })}
            >
              <DefaultBox
                bgcolor="primary.light"
                dark
                data={_.pick(client, ["id", "name", "email"])}
              />
            </Link>
          </Card>
        ))}
      </MainCard>
      <ListPrevNextBtn page={page} disabledPrev={page === 0} disabledNext={!hasMore} />
    </>
  )
}
const ClientsPage = () => {
  return (
    <Layout>
      <Head>
        <title>Clients</title>
      </Head>

      <div>
        <Btn href={Routes.NewClientPage()}>Create Client</Btn>

        <Suspense fallback={<div>Loading...</div>}>
          <ClientsList />
        </Suspense>
      </div>
    </Layout>
  )
}
export default ClientsPage
