import { Routes } from "@blitzjs/next"
import { useRouter } from "next/router"
import { useMutation } from "@blitzjs/rpc"
import Layout from "src/core/layouts/Layout"
import createClient from "src/clients/mutations/createClient"
import { ClientForm, FORM_ERROR } from "src/clients/components/ClientForm"
import { CLIENT_ROLES, USER_ROLES } from "src/utils/enums"
import { CreateClient } from "src/auth/validations"
import PrevBtn from "src/ui-component/buttons/PrevBtn"

const NewClientPage = () => {
  const router = useRouter()
  const [createClientMutation] = useMutation(createClient)
  return (
    <Layout title={"Create New Client"}>
      <h1>Create New Client</h1>

      <ClientForm
        submitText="Create Client"
        schema={CreateClient}
        initialValues={{ role: CLIENT_ROLES.CLIENT }}
        onSubmit={async (values) => {
          values.role = CLIENT_ROLES.CLIENT
          try {
            const client = await createClientMutation(values)
            await router.push(
              Routes.ShowClientPage({
                clientId: client.id,
              })
            )
          } catch (error) {
            console.error(error)
            return {
              [FORM_ERROR]: error.toString(),
            }
          }
        }}
      />

      <PrevBtn href={Routes.UsersPage()}>Clients</PrevBtn>
    </Layout>
  )
}

NewClientPage.authenticate = { role: [USER_ROLES.ADMIN, USER_ROLES.TRADER, USER_ROLES.COMMERCIAL] }
export default NewClientPage
