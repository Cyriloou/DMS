import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { useRouter } from "next/router"
import { useQuery, useMutation } from "@blitzjs/rpc"
import { useParam } from "@blitzjs/next"
import Layout from "src/core/layouts/Layout"
import getClient from "src/clients/queries/getClient"
import updateClient from "src/clients/mutations/updateClient"
import { ClientForm, FORM_ERROR } from "src/clients/components/ClientForm"
export const EditClient = () => {
  const router = useRouter()
  const clientId = useParam("clientId", "number")
  const [client, { setQueryData }] = useQuery(
    getClient,
    {
      id: clientId,
    },
    {
      // This ensures the query never refreshes and overwrites the form data while the user is editing.
      staleTime: Infinity,
    }
  )
  const [updateClientMutation] = useMutation(updateClient)
  return (
    <>
      <Head>
        <title>Edit Client {client.id}</title>
      </Head>

      <div>
        <h1>Edit Client {client.id}</h1>
        <pre>{JSON.stringify(client, null, 2)}</pre>

        <ClientForm
          submitText="Update Client"
          // TODO use a zod schema for form validation
          //  - Tip: extract mutation's schema into a shared `validations.ts` file and
          //         then import and use it here
          // schema={UpdateClient}
          initialValues={client}
          onSubmit={async (values) => {
            try {
              const updated = await updateClientMutation({
                id: client.id,
                ...values,
              })
              await setQueryData(updated)
              await router.push(
                Routes.ShowClientPage({
                  clientId: updated.id,
                })
              )
            } catch (error) {
              console.error(error)
              return {
                [FORM_ERROR]: error.toString(),
              }
            }
          }}
        />
      </div>
    </>
  )
}
const EditClientPage = () => {
  return (
    <div>
      <Suspense fallback={<div>Loading...</div>}>
        <EditClient />
      </Suspense>

      <p>
        <Link href={Routes.ClientsPage()}>Clients</Link>
      </p>
    </div>
  )
}
EditClientPage.authenticate = true
EditClientPage.getLayout = (page) => <Layout>{page}</Layout>
export default EditClientPage
