import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { useRouter } from "next/router"
import { useQuery, useMutation } from "@blitzjs/rpc"
import { useParam } from "@blitzjs/next"
import Layout from "src/core/layouts/Layout"
import getClient from "src/clients/queries/getClient"
import deleteClient from "src/clients/mutations/deleteClient"
import EditBtn from "src/ui-component/buttons/EditBtn"
import DeleteBtn from "src/ui-component/buttons/DeleteBtn"
import PrevBtn from "src/ui-component/buttons/PrevBtn"
export const Client = () => {
  const router = useRouter()
  const clientId = useParam("clientId", "number")
  const [deleteClientMutation] = useMutation(deleteClient)
  const [client] = useQuery(getClient, {
    id: clientId,
  })
  return (
    <>
      <Head>
        <title>Client {client.id}</title>
      </Head>

      <div>
        <h1>Client {client.id}</h1>
        <h2>{client.name}</h2>

        <EditBtn
          href={Routes.EditClientPage({
            clientId: client.id,
          })}
        />

        <DeleteBtn
          onClick={async () => {
            if (window.confirm("This will be deleted")) {
              await deleteClientMutation({
                id: client.id,
              })
              await router.push(Routes.ClientsPage())
            }
          }}
        />
      </div>
    </>
  )
}
const ShowClientPage = () => {
  return (
    <div>
      <PrevBtn href={Routes.ClientsPage()}>Clients</PrevBtn>

      <Suspense fallback={<div>Loading...</div>}>
        <Client />
      </Suspense>
    </div>
  )
}
ShowClientPage.authenticate = true
ShowClientPage.getLayout = (page) => <Layout>{page}</Layout>
export default ShowClientPage
