import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { useRouter } from "next/router"
import { useQuery, useMutation } from "@blitzjs/rpc"
import { useParam } from "@blitzjs/next"
import Layout from "src/core/layouts/Layout"
import getWood from "src/woods/queries/getWood"
import updateWood from "src/woods/mutations/updateWood"
import { WoodForm, FORM_ERROR } from "src/woods/components/WoodForm"
import PrevBtn from "src/ui-component/buttons/PrevBtn"
import { UpdateWood } from "src/auth/validations"
import { USER_ROLES } from "src/utils/enums"
export const EditWood = () => {
  const router = useRouter()
  const woodId = useParam("woodId", "number")
  const [wood, { setQueryData }] = useQuery(
    getWood,
    {
      id: woodId,
    },
    {
      // This ensures the query never refreshes and overwrites the form data while the user is editing.
      staleTime: Infinity,
    }
  )
  const [updateWoodMutation] = useMutation(updateWood)
  return (
    <>
      <Head>
        <title>Edit Wood {wood.id}</title>
      </Head>

      <div>
        <h1>Edit Wood {wood.id}</h1>

        <WoodForm
          submitText="Update Wood"
          // TODO use a zod schema for form validation
          //  - Tip: extract mutation's schema into a shared `validations.ts` file and
          //         then import and use it here
          schema={UpdateWood}
          initialValues={wood}
          onSubmit={async (values) => {
            try {
              const updated = await updateWoodMutation({
                id: wood.id,
                ...values,
              })
              await setQueryData(updated)
              await router.push(
                Routes.ShowWoodPage({
                  woodId: updated.id,
                })
              )
            } catch (error) {
              console.error(error)
              return {
                [FORM_ERROR]: error.toString(),
              }
            }
          }}
        />
      </div>
    </>
  )
}
const EditWoodPage = () => {
  return (
    <div>
      <Suspense fallback={<div>Loading...</div>}>
        <EditWood />
      </Suspense>

      <PrevBtn href={Routes.WoodsPage()}>Woods</PrevBtn>
    </div>
  )
}
EditWoodPage.authenticate = { role: [USER_ROLES.ADMIN, USER_ROLES.TRADER] }
EditWoodPage.getLayout = (page) => <Layout>{page}</Layout>
export default EditWoodPage
