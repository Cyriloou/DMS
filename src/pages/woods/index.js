import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { usePaginatedQuery } from "@blitzjs/rpc"
import { useRouter } from "next/router"
import Layout from "src/core/layouts/Layout"
import getWoods from "src/woods/queries/getWoods"
import ListPrevNextBtn from "src/ui-component/list/ListPrevNextBtn"
import Btn from "src/ui-component/buttons/Btn"
import MainCard from "src/ui-component/cards/MainCard"
import { Card } from "@mui/material"
import DefaultBox from "src/ui-component/DefaultBox"
import _ from "lodash"

const ITEMS_PER_PAGE = 100
export const WoodsList = () => {
  const router = useRouter()
  const page = Number(router.query.page) || 0
  const [{ woods, hasMore }] = usePaginatedQuery(getWoods, {
    orderBy: {
      id: "asc",
    },
    skip: ITEMS_PER_PAGE * page,
    take: ITEMS_PER_PAGE,
  })
  return (
    <div>
      <MainCard title="Wood">
        {woods.map((wood) => (
          <Card sx={{ overflow: "hidden" }} key={wood.id}>
            <Link
              href={Routes.ShowWoodPage({
                woodId: wood.id,
              })}
            >
              <DefaultBox bgcolor="primary.light" dark data={_.pick(wood, ["id", "name"])} />
            </Link>
          </Card>
        ))}
      </MainCard>
      <ListPrevNextBtn page={page} disabledPrev={page === 0} disabledNext={!hasMore} />
    </div>
  )
}
const WoodsPage = () => {
  return (
    <Layout>
      <Head>
        <title>Woods</title>
      </Head>

      <div>
        <Btn href={Routes.NewWoodPage()}>Create Wood</Btn>

        <Suspense fallback={<div>Loading...</div>}>
          <WoodsList />
        </Suspense>
      </div>
    </Layout>
  )
}
export default WoodsPage
