import { Routes } from "@blitzjs/next"
import Link from "next/link"
import { useRouter } from "next/router"
import { useMutation } from "@blitzjs/rpc"
import Layout from "src/core/layouts/Layout"
import createWood from "src/woods/mutations/createWood"
import { WoodForm, FORM_ERROR } from "src/woods/components/WoodForm"
import PrevBtn from "src/ui-component/buttons/PrevBtn"
import { CreateWood } from "src/auth/validations"
import { USER_ROLES } from "src/utils/enums"
const NewWoodPage = () => {
  const router = useRouter()
  const [createWoodMutation] = useMutation(createWood)
  return (
    <Layout title={"Create New Wood"}>
      <h1>Create New Wood</h1>

      <WoodForm
        submitText="Create Wood"
        schema={CreateWood}
        onSubmit={async (values) => {
          try {
            const wood = await createWoodMutation(values)
            await router.push(
              Routes.ShowWoodPage({
                woodId: wood.id,
              })
            )
          } catch (error) {
            console.error(error)
            return {
              [FORM_ERROR]: error.toString(),
            }
          }
        }}
      />

      <PrevBtn href={Routes.WoodsPage()}>Woods</PrevBtn>
    </Layout>
  )
}
NewWoodPage.authenticate = { role: [USER_ROLES.ADMIN, USER_ROLES.TRADER] }
export default NewWoodPage
