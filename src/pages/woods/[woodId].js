import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { useRouter } from "next/router"
import { useQuery, useMutation } from "@blitzjs/rpc"
import { useParam } from "@blitzjs/next"
import Layout from "src/core/layouts/Layout"
import getWood from "src/woods/queries/getWood"
import deleteWood from "src/woods/mutations/deleteWood"
import { USER_ROLES } from "src/utils/enums"
import EditBtn from "src/ui-component/buttons/EditBtn"
import DeleteBtn from "src/ui-component/buttons/DeleteBtn"
import PrevBtn from "src/ui-component/buttons/PrevBtn"
export const Wood = () => {
  const router = useRouter()
  const woodId = useParam("woodId", "number")
  const [deleteWoodMutation] = useMutation(deleteWood)
  const [wood] = useQuery(getWood, {
    id: woodId,
  })
  return (
    <>
      <Head>
        <title>Wood {wood.id}</title>
      </Head>

      <div>
        <h1>Wood {wood.id}</h1>
        <h2>{wood.name}</h2>

        <EditBtn
          href={Routes.EditWoodPage({
            woodId: wood.id,
          })}
        />

        <DeleteBtn
          onClick={async () => {
            if (window.confirm("This will be deleted")) {
              await deleteWoodMutation({
                id: wood.id,
              })
              await router.push(Routes.WoodsPage())
            }
          }}
        />
      </div>
    </>
  )
}
const ShowWoodPage = () => {
  return (
    <div>
      <PrevBtn href={Routes.WoodsPage()}>Woods</PrevBtn>

      <Suspense fallback={<div>Loading...</div>}>
        <Wood />
      </Suspense>
    </div>
  )
}
ShowWoodPage.authenticate = { role: [USER_ROLES.ADMIN, USER_ROLES.TRADER] }
ShowWoodPage.getLayout = (page) => <Layout>{page}</Layout>
export default ShowWoodPage
