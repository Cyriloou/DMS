import { ErrorComponent, ErrorBoundary, Routes } from "@blitzjs/next"
import { Grid, ThemeProvider } from "@mui/material"
import { AuthenticationError, AuthorizationError } from "blitz"
import Link from "next/link"
import React, { Suspense } from "react"
import { withBlitz } from "src/blitz-client"
import MainLayout from "src/core/layouts/MainLayout"
import "src/styles/globals.css"
import "src/styles/scss/style.scss"
import Btn from "src/ui-component/buttons/Btn"
import { localStorageName } from "src/utils/constant"
import { useLocalStorage } from "src/utils/hooks/useLocalStorage"
import theme from "src/utils/themes"

function RootErrorFallback({ error }) {
  if (error instanceof AuthenticationError) {
    return (
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        width={"100vw"}
        height={"100vh"}
      >
        <Grid justifyContent="center">
          <div>Error: You are not authenticated</div>
          <Btn href={Routes.Home()}>Go Home</Btn>
        </Grid>
      </Grid>
    )
  } else if (error instanceof AuthorizationError) {
    return (
      <ErrorComponent
        statusCode={error.statusCode}
        title="Sorry, you are not authorized to access this"
      />
    )
  } else {
    return (
      <ErrorComponent statusCode={error?.statusCode || 400} title={error.message || error.name} />
    )
  }
}
function MyApp({ Component, pageProps }) {
  const [localStorage] = useLocalStorage(localStorageName)
  const customization = localStorage?.customization || {}
  const getLayout =
    Component.getLayout ||
    ((page) => (
      <Suspense fallback="Loading...">
        <MainLayout>{page}</MainLayout>
      </Suspense>
    ))
  return (
    <ErrorBoundary FallbackComponent={RootErrorFallback}>
      <ThemeProvider theme={theme(customization)}>
        {getLayout(<Component {...pageProps} />)}
      </ThemeProvider>
    </ErrorBoundary>
  )
}
export default withBlitz(MyApp)
