import Layout from "src/core/layouts/Layout"
import { LabeledTextField } from "src/core/components/LabeledTextField"
import { Form, FORM_ERROR } from "src/core/components/Form"
import { ForgotPassword } from "src/auth/validations"
import forgotPassword from "src/auth/mutations/forgotPassword"
import { useMutation } from "@blitzjs/rpc"
import { Grid } from "@mui/material"
import AuthWrapper1 from "./AuthWrapper1"
const ForgotPasswordPage = () => {
  const [forgotPasswordMutation, { isSuccess }] = useMutation(forgotPassword)
  return (
    <AuthWrapper1>
      <Grid container direction="column" justifyContent="flex-end" sx={{ minHeight: "100vh" }}>
        <Grid item xs={12}>
          <Grid
            container
            justifyContent="center"
            alignItems="center"
            sx={{ minHeight: "calc(100vh - 68px)" }}
          >
            <Grid item sx={{ m: { xs: 1, sm: 3 }, mb: 0 }}>
              <h1>Forgot your password?</h1>
              {isSuccess ? (
                <div>
                  <h2>Request Submitted</h2>
                  <p>
                    If your email is in our system, you will receive instructions to reset your
                    password shortly.
                  </p>
                </div>
              ) : (
                <Form
                  submitText="Send Reset Password Instructions"
                  schema={ForgotPassword}
                  initialValues={{
                    email: "",
                  }}
                  onSubmit={async (values) => {
                    try {
                      await forgotPasswordMutation(values)
                    } catch (error) {
                      return {
                        [FORM_ERROR]: "Sorry, we had an unexpected error. Please try again.",
                      }
                    }
                  }}
                >
                  <LabeledTextField name="email" label="Email" placeholder="Email" />
                </Form>
              )}
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sx={{ m: 3, mt: 1 }}></Grid>
      </Grid>
    </AuthWrapper1>
  )
}
export default ForgotPasswordPage
