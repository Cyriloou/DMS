import { useRouter } from "next/router"
import { SignupForm } from "src/auth/components/SignupForm"
import { Routes } from "@blitzjs/next"
import AuthWrapper1 from "./AuthWrapper1"
import { Grid } from "@mui/material"

const SignupPage = () => {
  const router = useRouter()
  return (
    <AuthWrapper1>
      <Grid container direction="column" justifyContent="flex-end" sx={{ minHeight: "100vh" }}>
        <Grid item xs={12}>
          <Grid
            container
            justifyContent="center"
            alignItems="center"
            sx={{ minHeight: "calc(100vh - 68px)" }}
          >
            <Grid item sx={{ m: { xs: 1, sm: 3 }, mb: 0 }}>
              <SignupForm onSuccess={() => router.push(Routes.Home())} />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sx={{ m: 3, mt: 1 }}></Grid>
      </Grid>
    </AuthWrapper1>
  )
}
export default SignupPage
