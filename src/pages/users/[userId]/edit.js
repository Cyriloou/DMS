import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import { useRouter } from "next/router"
import { useQuery, useMutation } from "@blitzjs/rpc"
import { useParam } from "@blitzjs/next"
import getUser from "src/users/queries/getUser"
import updateUser from "src/users/mutations/updateUser"
import { UserForm, FORM_ERROR } from "src/users/components/UserForm"
import { UpdateUser } from "src/auth/validations"
import PrevBtn from "src/ui-component/buttons/PrevBtn"
import { USER_ROLES } from "src/utils/enums"
import AuthWrapper1 from "src/pages/auth/AuthWrapper1"
export const EditUser = () => {
  const router = useRouter()
  const userId = useParam("userId", "number")
  const [user, { setQueryData }] = useQuery(
    getUser,
    {
      id: userId,
    },
    {
      // This ensures the query never refreshes and overwrites the form data while the user is editing.
      staleTime: Infinity,
    }
  )
  const [updateUserMutation] = useMutation(updateUser)
  return (
    <>
      <Head>
        <title>Edit User {user.id}</title>
      </Head>

      <div>
        <h1>Edit User {user.id}</h1>
        <UserForm
          submitText="Update User"
          // TODO use a zod schema for form validation
          //  - Tip: extract mutation's schema into a shared `validations.ts` file and
          //         then import and use it here
          schema={UpdateUser}
          initialValues={user}
          onSubmit={async (values) => {
            try {
              const updated = await updateUserMutation({
                id: user.id,
                ...values,
              })
              await setQueryData(updated)
              await router.push(
                Routes.ShowUserPage({
                  userId: updated.id,
                })
              )
            } catch (error) {
              console.error(error)
              return {
                [FORM_ERROR]: error.toString(),
              }
            }
          }}
        />
      </div>
    </>
  )
}
const EditUserPage = () => {
  return (
    <div>
      <Suspense fallback={<div>Loading...</div>}>
        <EditUser />
      </Suspense>

      <PrevBtn href={Routes.UsersPage()}>Users</PrevBtn>
    </div>
  )
}
EditUserPage.authenticate = { role: [USER_ROLES.ADMIN, USER_ROLES.TRADER] }
EditUserPage.getLayout = (page) => <AuthWrapper1>{page}</AuthWrapper1>
export default EditUserPage
