import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import { useRouter } from "next/router"
import { useQuery, useMutation } from "@blitzjs/rpc"
import { useParam } from "@blitzjs/next"
import Layout from "src/core/layouts/Layout"
import getUser from "src/users/queries/getUser"
import deleteUser from "src/users/mutations/deleteUser"
import { USER_ROLES } from "src/utils/enums"
import DeleteBtn from "src/ui-component/buttons/DeleteBtn"
import EditBtn from "src/ui-component/buttons/EditBtn"
import PrevBtn from "src/ui-component/buttons/PrevBtn"
import MainLayout from "src/core/layouts/MainLayout"
import FormLayout from "src/core/layouts/FormLayout"

export const User = () => {
  const router = useRouter()
  const userId = useParam("userId", "number")
  const [deleteUserMutation] = useMutation(deleteUser)
  const [user] = useQuery(getUser, {
    id: userId,
  })
  return (
    <>
      <Head>
        <title>User {user.id}</title>
      </Head>

      <div>
        <h1>User {user.id}</h1>
        <h2>{user.email}</h2>

        <EditBtn
          href={Routes.EditUserPage({
            userId: user.id,
          })}
        />

        <DeleteBtn
          onClick={async () => {
            if (window.confirm("This will be deleted")) {
              await deleteUserMutation({
                id: user.id,
              })
              await router.push(Routes.UsersPage())
            }
          }}
        />
      </div>
    </>
  )
}
const ShowUserPage = () => {
  return (
    <div>
      <PrevBtn href={Routes.UsersPage()} title={"Users"} />

      <Suspense fallback={<div>Loading...</div>}>
        <User />
      </Suspense>
    </div>
  )
}
ShowUserPage.authenticate = { role: [USER_ROLES.ADMIN, USER_ROLES.TRADER] }
ShowUserPage.getLayout = (page) => <FormLayout>{page}</FormLayout>
export default ShowUserPage
