import { Routes } from "@blitzjs/next"
import { useRouter } from "next/router"
import { useMutation } from "@blitzjs/rpc"
import Layout from "src/core/layouts/Layout"
import createUser from "src/users/mutations/createUser"
import { UserForm, FORM_ERROR } from "src/users/components/UserForm"
import { CreateUser } from "src/auth/validations"
import { USER_ROLES } from "src/utils/enums"
import PrevBtn from "src/ui-component/buttons/PrevBtn"

const NewUserPage = () => {
  const router = useRouter()
  const [createUserMutation] = useMutation(createUser)
  return (
    <Layout title={"Create New User"}>
      <h1>Create New User</h1>

      <UserForm
        submitText="Create User"
        schema={CreateUser}
        initialValues={{
          role: USER_ROLES.USER,
        }}
        onSubmit={async (values) => {
          try {
            const user = await createUserMutation(values)
            await router.push(
              Routes.ShowUserPage({
                userId: user.id,
              })
            )
          } catch (error) {
            console.error(error)
            return {
              [FORM_ERROR]: error.toString(),
            }
          }
        }}
      />

      <PrevBtn href={Routes.UsersPage()}>Users</PrevBtn>
    </Layout>
  )
}

NewUserPage.authenticate = { role: [USER_ROLES.ADMIN, USER_ROLES.TRADER] }
export default NewUserPage
