import PropTypes from "prop-types"
import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { usePaginatedQuery } from "@blitzjs/rpc"
import { useRouter } from "next/router"
import Layout from "src/core/layouts/Layout"
import getUsers from "src/users/queries/getUsers"
import MainCard from "src/ui-component/cards/MainCard"
import { Box, Card, Grid, Typography } from "@mui/material"
import ListPrevNextBtn from "src/ui-component/list/ListPrevNextBtn"
import Btn from "src/ui-component/buttons/Btn"
const ITEMS_PER_PAGE = 100
export const UsersList = () => {
  const router = useRouter()
  const page = Number(router.query.page) || 0
  const [{ users, hasMore }] = usePaginatedQuery(getUsers, {
    orderBy: {
      id: "asc",
    },
    skip: ITEMS_PER_PAGE * page,
    take: ITEMS_PER_PAGE,
  })
  const UserBox = ({ bgcolor, name, email, dark }) => (
    <Card sx={{ mb: 3 }}>
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          py: 4.5,
          bgcolor,
          color: dark ? "grey.800" : "#ffffff",
        }}
      >
        <Grid item xs={12}>
          <Grid>
            {name && (
              <Typography variant="subtitle1" color="inherit">
                {name}
              </Typography>
            )}
            {!name && <Box sx={{ p: 1.15 }} />}
          </Grid>
          <Grid>
            {email && (
              <Typography variant="subtitle1" color="inherit">
                {email}
              </Typography>
            )}
            {!email && <Box sx={{ p: 1.15 }} />}
          </Grid>
        </Grid>
      </Box>
    </Card>
  )

  UserBox.propTypes = {
    bgcolor: PropTypes.string,
    name: PropTypes.string,
    email: PropTypes.string,
    dark: PropTypes.bool,
  }
  return (
    <>
      <MainCard title="Users">
        {users.map((user) => (
          <Card sx={{ overflow: "hidden" }} key={user.id}>
            <Link
              href={Routes.ShowUserPage({
                userId: user.id,
              })}
            >
              <UserBox
                bgcolor="primary.light"
                dark
                name={user.name || `user_${user.id}`}
                email={user.email}
              />
            </Link>
          </Card>
        ))}
      </MainCard>
      <ListPrevNextBtn page={page} disabledPrev={page === 0} disabledNext={!hasMore} />
    </>
  )
}
const UsersPage = () => {
  return (
    <Layout>
      <Head>
        <title>Users</title>
      </Head>

      <div>
        <Btn href={Routes.NewUserPage()}>Create User</Btn>

        <Suspense fallback={<div>Loading...</div>}>
          <UsersList />
        </Suspense>
      </div>
    </Layout>
  )
}
export default UsersPage
