import { Suspense } from "react"
import Link from "next/link"
import { useCurrentUser } from "src/users/hooks/useCurrentUser"
import logout from "src/auth/mutations/logout"
import { useMutation } from "@blitzjs/rpc"
import { Routes } from "@blitzjs/next"
import styles from "src/styles/Home.module.css"
import Logo from "src/ui-component/Logo"
import { IconButton } from "@mui/material"
import LogoutIcon from "@mui/icons-material/Logout"
import Layout from "src/core/layouts/Layout"

/*
 * This file is just for a pleasant getting started page for your new app.
 * You can delete everything in here and start from scratch if you like.
 */

const UserInfo = () => {
  const currentUser = useCurrentUser()
  const [logoutMutation] = useMutation(logout)
  if (currentUser) {
    return (
      <>
        <IconButton
          aria-label="edit"
          size="large"
          type="button"
          onClick={async () => {
            await logoutMutation()
          }}
          style={{
            marginLeft: "0.5rem",
          }}
          color="secondary"
        >
          <LogoutIcon fontSize="inherit" />
        </IconButton>
        <div>
          User id: <code>{currentUser.id}</code>
          <br />
          User role: <code>{currentUser.role}</code>
        </div>
      </>
    )
  } else {
    return (
      <>
        <Link href={Routes.SignupPage()} className={styles.button}>
          <strong>Sign Up</strong>
        </Link>
        <Link href={Routes.LoginPage()} className={styles.loginButton}>
          <strong>Login</strong>
        </Link>
      </>
    )
  }
}
const Home = () => {
  return (
    <Layout title="Home">
      <div className={styles.globe} />

      <div className={styles.container}>
        <div className={styles.toastContainer}>
          <p>
            <strong>Woodstock!</strong> your new dealing experience.
          </p>
        </div>

        <main className={styles.main}>
          <div className={styles.wrapper}>
            <div className={styles.header}>
              <div className={styles.logo}>
                <Logo />
              </div>

              <h1>Woodstock</h1>

              {/* Auth */}

              <div className={styles.buttonContainer}>
                <Suspense fallback="Loading...">
                  <UserInfo />
                </Suspense>
              </div>
            </div>
          </div>
        </main>

        <footer className={styles.footer}>
          <span>Powered by</span>
          <a
            href="https://www.linkedin.com/in/cyrillacheretz/"
            target="_blank"
            rel="noopener noreferrer"
            className={styles.textLink}
          >
            Cyril
          </a>
        </footer>
      </div>
    </Layout>
  )
}
export default Home
