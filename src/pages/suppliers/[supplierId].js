import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { useRouter } from "next/router"
import { useQuery, useMutation } from "@blitzjs/rpc"
import { useParam } from "@blitzjs/next"
import Layout from "src/core/layouts/Layout"
import deleteClient from "src/clients/mutations/deleteClient"
import EditBtn from "src/ui-component/buttons/EditBtn"
import DeleteBtn from "src/ui-component/buttons/DeleteBtn"
import getSupplier from "src/clients/queries/getSupplier"
import PrevBtn from "src/ui-component/buttons/PrevBtn"
import { USER_ROLES } from "src/utils/enums"
export const Supplier = () => {
  const router = useRouter()
  const supplierId = useParam("supplierId", "number")
  const [deleteSupplierMutation] = useMutation(deleteClient)

  const [supplier] = useQuery(getSupplier, {
    id: supplierId,
  })
  return (
    <>
      <Head>
        <title>Supplier {supplier.id}</title>
      </Head>

      <div>
        <h1>Supplier {supplier.id}</h1>
        <h2>{supplier.name}</h2>

        <EditBtn
          href={Routes.EditSupplierPage({
            supplierId: supplier.id,
          })}
        />

        <DeleteBtn
          onClick={async () => {
            if (window.confirm("This will be deleted")) {
              await deleteSupplierMutation({
                id: supplier.id,
              })
              await router.push(Routes.SuppliersPage())
            }
          }}
        />
      </div>
    </>
  )
}
const ShowSupplierPage = () => {
  return (
    <div>
      <PrevBtn href={Routes.SuppliersPage()}>Suppliers</PrevBtn>

      <Suspense fallback={<div>Loading...</div>}>
        <Supplier />
      </Suspense>
    </div>
  )
}
ShowSupplierPage.authenticate = {
  role: [USER_ROLES.ADMIN, USER_ROLES.TRADER, USER_ROLES.COMMERCIAL],
}
ShowSupplierPage.getLayout = (page) => <Layout>{page}</Layout>
export default ShowSupplierPage
