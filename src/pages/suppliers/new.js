import { Routes } from "@blitzjs/next"
import { useRouter } from "next/router"
import { useMutation } from "@blitzjs/rpc"
import Layout from "src/core/layouts/Layout"
import createClient from "src/clients/mutations/createClient"
import { SupplierForm, FORM_ERROR } from "src/suppliers/components/SupplierForm"
import { CLIENT_ROLES, USER_ROLES } from "src/utils/enums"
import { CreateSupplier } from "src/auth/validations"
import PrevBtn from "src/ui-component/buttons/PrevBtn"

const NewSupplierPage = () => {
  const router = useRouter()
  const [createSupplierMutation] = useMutation(createClient)
  return (
    <Layout title={"Create New Supplier"}>
      <h1>Create New Supplier</h1>

      <SupplierForm
        submitText="Create Supplier"
        schema={CreateSupplier}
        initialValues={{ role: CLIENT_ROLES.SUPPLIER }}
        onSubmit={async (values) => {
          values.role = CLIENT_ROLES.SUPPLIER
          try {
            const supplier = await createSupplierMutation(values)
            await router.push(
              Routes.ShowSupplierPage({
                supplierId: supplier.id,
              })
            )
          } catch (error) {
            console.error(error)
            return {
              [FORM_ERROR]: error.toString(),
            }
          }
        }}
      />

      <PrevBtn href={Routes.UsersPage()}>Suppliers</PrevBtn>
    </Layout>
  )
}

NewSupplierPage.authenticate = {
  role: [USER_ROLES.ADMIN, USER_ROLES.TRADER, USER_ROLES.COMMERCIAL],
}
export default NewSupplierPage
