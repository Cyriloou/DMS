import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { usePaginatedQuery } from "@blitzjs/rpc"
import { useRouter } from "next/router"
import Layout from "src/core/layouts/Layout"
import Btn from "src/ui-component/buttons/Btn"
import ListPrevNextBtn from "src/ui-component/list/ListPrevNextBtn"
import { Card } from "@mui/material"
import MainCard from "src/ui-component/cards/MainCard"
import getSuppliers from "src/clients/queries/getSuppliers"
import _ from "lodash"
import DefaultBox from "src/ui-component/DefaultBox"

const ITEMS_PER_PAGE = 100

export const SuppliersList = () => {
  const router = useRouter()
  const page = Number(router.query.page) || 0
  const [{ suppliers, hasMore }] = usePaginatedQuery(getSuppliers, {
    orderBy: {
      id: "asc",
    },
    skip: ITEMS_PER_PAGE * page,
    take: ITEMS_PER_PAGE,
  })
  console.log("first", { suppliers, hasMore })
  return (
    <>
      <MainCard title="Suppliers">
        {suppliers.map((supplier) => (
          <Card sx={{ overflow: "hidden" }} key={supplier.id}>
            <Link
              href={Routes.ShowSupplierPage({
                supplierId: supplier.id,
              })}
            >
              <DefaultBox
                bgcolor="primary.light"
                dark
                data={_.pick(supplier, ["id", "name", "email"])}
              />
            </Link>
          </Card>
        ))}
      </MainCard>
      <ListPrevNextBtn page={page} disabledPrev={page === 0} disabledNext={!hasMore} />
    </>
  )
}
const SuppliersPage = () => {
  return (
    <Layout>
      <Head>
        <title>Suppliers</title>
      </Head>

      <div>
        <Btn href={Routes.NewSupplierPage()}>Create Supplier</Btn>

        <Suspense fallback={<div>Loading...</div>}>
          <SuppliersList />
        </Suspense>
      </div>
    </Layout>
  )
}
export default SuppliersPage
