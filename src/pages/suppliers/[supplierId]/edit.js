import { Suspense } from "react"
import { Routes } from "@blitzjs/next"
import Head from "next/head"
import Link from "next/link"
import { useRouter } from "next/router"
import { useQuery, useMutation } from "@blitzjs/rpc"
import { useParam } from "@blitzjs/next"
import Layout from "src/core/layouts/Layout"
import { SupplierForm, FORM_ERROR } from "src/suppliers/components/SupplierForm"
import getSupplier from "src/clients/queries/getSupplier"
import updateClient from "src/clients/mutations/updateClient"
import PrevBtn from "src/ui-component/buttons/PrevBtn"
import { USER_ROLES } from "src/utils/enums"

export const EditSupplier = () => {
  const router = useRouter()
  const supplierId = useParam("supplierId", "number")
  const [supplier, { setQueryData }] = useQuery(
    getSupplier,
    {
      id: supplierId,
    },
    {
      // This ensures the query never refreshes and overwrites the form data while the user is editing.
      staleTime: Infinity,
    }
  )
  const [updateSupplierMutation] = useMutation(updateClient)
  return (
    <>
      <Head>
        <title>Edit Supplier {supplier.id}</title>
      </Head>

      <div>
        <h1>Edit Supplier {supplier.id}</h1>
        <pre>{JSON.stringify(supplier, null, 2)}</pre>

        <SupplierForm
          submitText="Update Supplier"
          // TODO use a zod schema for form validation
          //  - Tip: extract mutation's schema into a shared `validations.ts` file and
          //         then import and use it here
          // schema={UpdateSupplier}
          initialValues={supplier}
          onSubmit={async (values) => {
            try {
              const updated = await updateSupplierMutation({
                id: supplier.id,
                ...values,
              })
              await setQueryData(updated)
              await router.push(
                Routes.ShowSupplierPage({
                  supplierId: updated.id,
                })
              )
            } catch (error) {
              console.error(error)
              return {
                [FORM_ERROR]: error.toString(),
              }
            }
          }}
        />
      </div>
    </>
  )
}
const EditSupplierPage = () => {
  return (
    <div>
      <Suspense fallback={<div>Loading...</div>}>
        <EditSupplier />
      </Suspense>

      <PrevBtn href={Routes.SuppliersPage()}>Suppliers</PrevBtn>
    </div>
  )
}
EditSupplierPage.authenticate = {
  role: [USER_ROLES.ADMIN, USER_ROLES.TRADER, USER_ROLES.COMMERCIAL],
}
EditSupplierPage.getLayout = (page) => <Layout>{page}</Layout>
export default EditSupplierPage
