import { Card, Grid, Typography } from "@mui/material"
import { Box } from "@mui/system"
import React from "react"
const DefaultBox = ({ bgcolor, data, dark }) => (
  <Card sx={{ mb: 3 }}>
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        py: 4.5,
        bgcolor,
        color: dark ? "grey.800" : "#ffffff",
      }}
    >
      <Grid container direction="row" justifyContent="center" alignItems="center" xs={12}>
        <Grid item>
          {Object.keys(data).map((key) => {
            return data[key] ? (
              <Typography variant="subtitle1" color="inherit">
                {data[key]}
              </Typography>
            ) : (
              <Box sx={{ p: 1.15 }} />
            )
          })}
        </Grid>
      </Grid>
    </Box>
  </Card>
)
export default DefaultBox
