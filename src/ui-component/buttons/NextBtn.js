import React from "react"
import { Button } from "@mui/material"
import ChevronRightIcon from "@mui/icons-material/ChevronRight"
import { useRouter } from "next/router"

function NextBtn({ onClick, disabled, title = "Next", href, children, ...props }) {
  const router = useRouter()
  const handleClick = (e) => {
    e.preventDefault()
    if (href) {
      router.push(href)
    }
    onClick && onClick(e)
  }
  return (
    <Button
      variant="text"
      disabled={disabled}
      onClick={handleClick}
      endIcon={<ChevronRightIcon />}
      {...props}
    >
      {children || title}
    </Button>
  )
}

export default NextBtn
