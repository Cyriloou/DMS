import React from "react"
import { IconButton } from "@mui/material"
import DeleteIcon from "@mui/icons-material/Delete"

function DeleteBtn({ onClick, title = "Btn", children, ...props }) {
  return (
    <IconButton
      aria-label="delete"
      size="large"
      type="button"
      onClick={onClick}
      style={{
        marginLeft: "0.5rem",
      }}
      variant="outlined"
      color="error"
      {...props}
    >
      <DeleteIcon fontSize="inherit" />
    </IconButton>
  )
}

export default DeleteBtn
