import React from "react"
import { Button } from "@mui/material"
import { useRouter } from "next/router"

function Btn({ onClick, title = "Btn", href, children, ...props }) {
  const router = useRouter()
  const handleClick = (e) => {
    e.preventDefault()
    if (href) {
      router.push(href)
    }
    onClick && onClick(e)
  }
  return (
    <Button variant="contained" {...props} onClick={handleClick}>
      {children || title}
    </Button>
  )
}

export default Btn
