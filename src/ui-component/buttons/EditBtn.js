import React from "react"
import { IconButton } from "@mui/material"
import EditIcon from "@mui/icons-material/Edit"
import { useRouter } from "next/router"

function EditBtn({ onClick, title = "Btn", href, children, ...props }) {
  const router = useRouter()
  const handleClick = (e) => {
    e.preventDefault()
    if (href) {
      router.push(href)
    }
    onClick && onClick(e)
  }
  return (
    <IconButton
      aria-label="edit"
      size="large"
      type="button"
      onClick={onClick}
      style={{
        marginLeft: "0.5rem",
      }}
      variant="outlined"
      color="secondary"
      onClick={handleClick}
      {...props}
    >
      <EditIcon fontSize="inherit" />
    </IconButton>
  )
}

export default EditBtn
