import React from "react"
import { Button } from "@mui/material"
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft"
import { useRouter } from "next/router"

function PrevBtn({ onClick, disabled, title = "Previous", href, children, ...props }) {
  const router = useRouter()
  const handleClick = (e) => {
    e.preventDefault()
    if (href) {
      router.push(href)
    }
    onClick && onClick(e)
  }
  return (
    <Button
      variant="text"
      disabled={disabled}
      onClick={handleClick}
      startIcon={<ChevronLeftIcon />}
      {...props}
    >
      {children || title}
    </Button>
  )
}

export default PrevBtn
