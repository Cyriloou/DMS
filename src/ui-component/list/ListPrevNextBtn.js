import { useRouter } from "next/router"
import React from "react"
import NextBtn from "src/ui-component/buttons/NextBtn"
import PrevBtn from "src/ui-component/buttons/PrevBtn"

function ListPrevNextBtn({ page, disabledPrev, disabledNext }) {
  const router = useRouter()
  const goToPreviousPage = () =>
    router.push({
      query: {
        page: page - 1,
      },
    })
  const goToNextPage = () => {
    router.push({
      query: {
        page: page + 1,
      },
    })
  }
  return (
    <>
      <PrevBtn disabled={disabledPrev} onClick={goToPreviousPage} />
      <NextBtn disabled={disabledNext} onClick={goToNextPage} />
    </>
  )
}

export default ListPrevNextBtn
